<?php

declare(strict_types=1);

use Shadon\DevTools\Command\CreatePhpunitCommand;
use Shadon\DevTools\Command\ModuleAclCommand;
use Shadon\DevTools\Command\RequestArgsCommand;
use Shadon\DevTools\Command\ReturnExplainCommand;
use Shadon\DevTools\Command\LogicCommand;
use Shadon\DevTools\Command\ControllerCommand;
use Shadon\DevTools\Command\ModelCommand;
use Shadon\DevTools\Command\RepositoryCommand;
use Shadon\DevTools\Command\EventListenerCommand;
use Shadon\DevTools\Command\ValidationCommand;
use Shadon\DevTools\Command\ModuleCommand;
use Shadon\DevTools\Command\CreateProjectCommand;
use Shadon\DevTools\Command\SetEnvCommand;
use Shadon\DevTools\Command\SdkCommand;
use Shadon\DevTools\Command\InitCommand;
use Shadon\DevTools\Command\ApiCommand;
use Shadon\DevTools\Command\ApiAclCommand;

return [
    'create-project' => CreateProjectCommand::class,
    'module' => ModuleCommand::class,
    'logic' => LogicCommand::class,
    'controller' => ControllerCommand::class,
    'model' => ModelCommand::class,
    'repository' => RepositoryCommand::class,
    'event-listener' => EventListenerCommand::class,
    'validation' => ValidationCommand::class,
    'create-phpunit' => CreatePhpunitCommand::class,
    'module-acl' => ModuleAclCommand::class,
    'request-args' => RequestArgsCommand::class,
    'return-explain' => ReturnExplainCommand::class,
    'set-env' => SetEnvCommand::class,
    'sdk' => SdkCommand::class,
    'init' => InitCommand::class,
    'api' => ApiCommand::class,
    'api-acl' => ApiAclCommand::class,
];