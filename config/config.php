<?php

declare(strict_types=1);

return [
    'command' => require 'command.php',
    'module' => require 'module.php',
];
