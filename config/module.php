<?php

declare(strict_types=1);

return [
    // 需构建的模块
    'buildModules' => [
        'oauth',
        'user',
        'store',
        'goods',
        'pay',
        'order',
        'service',
        'message',
        'system',
        'log',
    ],
    'dbHost'    => 'db',
    'dbUser'    => 'root',
    'dbPass'    => '123456',
    'dbPort'    => '3306',
    'dbCharset' => 'utf8',
    'dbPrefix'  => 'el_', //el_
    'oauthDb'   => [
        'host'     => 'db',
        'dbname'   => 'el_oauth',
        'port'     => '3306',
        'username' => 'root',
        'password' => '123456',
        'charset'  => 'utf8',
    ],
    // 是否提前生成logic,预生成的logic为空类,如需interface内的代码生成到logic设置为false即可
    'beforeLogic' => true,
];
