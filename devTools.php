#! /usr/bin/env php
<?php

declare(strict_types = 1);

define('ROOT_PATH', __DIR__);

require ROOT_PATH . '/vendor/autoload.php';

$bootstrap = new \Shadon\DevTools\Bootstrap();
$bootstrap->run();