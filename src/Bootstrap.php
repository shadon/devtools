<?php

declare(strict_types = 1);

namespace Shadon\DevTools;

use Phalcon\Di;
use Phalcon\Config;
use Symfony\Component\Console\Application;
use Phalcon\Di\Injectable;
use Phalcon\Loader;

class Bootstrap extends Injectable
{
    protected $di = null;

    private $logo = '  _____ _               _             _____          _______          _
 / ____| |             | |           |  __ \        |__   __|        | |
| (___ | |__   __ _  __| | ___  _ __ | |  | | _____   _| | ___   ___ | |___
 \___ \| \'_ \ / _` |/ _` |/ _ \| \'_ \| |  | |/ _ \ \ / / |/ _ \ / _ \| / __|
 ____) | | | | (_| | (_| | (_) | | | | |__| |  __/\ V /| | (_) | (_) | \__ \
|_____/|_| |_|\__,_|\__,_|\___/|_| |_|_____/ \___| \_/ |_|\___/ \___/|_|___/
';

    public function __construct()
    {
        $di = $this->di = new Di();
        $di->setShared('config', function(){
            return new Config(require dirname(__DIR__) . '/config/config.php');
        });
        $di->setShared('loader', function(){
            return new Loader();
        });
        $logo = $this->logo;
        $di->setShared('application', function() use ($logo){
            return new Application($logo);
        });
    }

    public function run()
    {
        $registerCommand = $this->config->command->toArray();
        if (empty($registerCommand)) {
            throw new \Symfony\Component\Console\Exception\RuntimeException('not found register command');
        }
        $registerCommands = [];
        array_walk($registerCommand, function ($command, $name) use (&$registerCommands){
            $registerCommands[] = $this->di->getShared($command, [$name]);
        });
        $this->application->addCommands($registerCommands);
        $this->application->run();
    }
}