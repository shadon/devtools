<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\BuildFile;

class ControllerFile extends File
{
    protected $controllerDir = '';

    protected $controllerNamespace = '';

    protected $extName = 'Controller';

    public function run(array $dirInfo, array $controllers): void
    {
        $this->setDirInfo($dirInfo);
        $controllers = array_map('ucfirst', preg_replace_callback('/(_)([a-z])/i', function ($matches) {
            return ucfirst($matches[2]);
        }, $controllers));
        $this->buildController($controllers);
    }

    /**
     * 设置目录/命名空间.
     *
     * @param array $dirInfo
     */
    private function setDirInfo(array $dirInfo): void
    {
        $this->controllerDir = $dirInfo['path'] ?? '';
        $this->controllerNamespace = $dirInfo['namespace'] ?? '';
    }

    private function buildController(array $controllers): void
    {
        !is_dir($this->controllerDir) && mkdir($this->controllerDir, 0755, true);
        foreach ($controllers as $controller) {
            $controllerName = $controller . $this->extName;
            $this->buildControllerFile($controllerName);
        }
    }

    private function buildControllerFile(string $controllerName): void
    {
        $filePath = sprintf('%s/%s%s',
                $this->controllerDir,
                $controllerName,
                $this->fileExt
            );
        if (!file_exists($filePath)) {
            $fileCode = $this->getControllerFileCode($controllerName);
            $fp = fopen($filePath, 'w');
            fwrite($fp, $fileCode);
        }
    }

    private function getControllerFileCode(string $controllerName): string
    {
        $templates = $this->getTemplateFile('Base');
        $namespace = $this->controllerNamespace;
        $useNamespace = [
            getenv('PROJECT_NAMESPACE') . '\Controller\Base\AbstractUserController',
        ];
        $className = $controllerName;
        $extendsName = 'AbstractUserController';
        $className = $this->getClassName($className, $extendsName, []);
        $namespace = $this->getNamespace($namespace);
        $useNamespace = $this->getUseNamespace($useNamespace);

        return sprintf($templates, $namespace, $useNamespace, $className, '', '');
    }
}