<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\BuildFile;

class EventListenerFile extends File
{
    protected $eventListenerDir = '';

    protected $eventListenerNamespace = '';

    protected $extName = 'EventListener';

    public function run(string $moduleName, array $dirInfo, array $tables = []): void
    {
        $this->setModuleName($moduleName);
        $this->setDirInfo($dirInfo);
        $eventListeners = $this->convertTableName($tables);
        $this->buildEventListener($eventListeners);
    }

    /**
     * 设置目录/命名空间.
     *
     * @param array $dirInfo
     */
    private function setDirInfo(array $dirInfo): void
    {
        $this->eventListenerDir = $dirInfo['path'] ?? '';
        $this->eventListenerNamespace = $dirInfo['namespace'] ?? '';
    }

    private function buildEventListener(array $eventListeners): void
    {
        !is_dir($this->eventListenerDir) && mkdir($this->eventListenerDir, 0755, true);
        foreach ($eventListeners as $eventListener) {
            $eventListenerName = $eventListener . $this->extName;
            $this->buildEventListenerFile($eventListenerName);
        }
    }

    private function buildEventListenerFile(string $eventListenerName): void
    {
        $filePath = sprintf('%s/%s%s',
            $this->eventListenerDir,
            $eventListenerName,
            $this->fileExt
            );
        if (!file_exists($filePath)) {
            $fileCode = $this->getEventListenerFileCode($eventListenerName);
            $fp = fopen($filePath, 'w');
            fwrite($fp, $fileCode);
        }
    }

    private function getEventListenerFileCode(string $eventListenerName): string
    {
        $templates = $this->getTemplateFile('Base');
        $namespace = $this->eventListenerNamespace;
        $className = $eventListenerName;
        $className = $this->getClassName($className, '', []);
        $namespace = $this->getNamespace($namespace);

        return sprintf($templates, $namespace, '', $className, '', '');
    }
}