<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\BuildFile;

class RepositoryFile extends File
{
    protected $repositoryDir = '';

    protected $repositoryNamespace = '';

    protected $extName = 'Repository';

    public function run(string $moduleName, array $dirInfo, array $tables): void
    {
        $this->setModuleName($moduleName);
        $this->setDirInfo($dirInfo);
        $repositorys = $this->convertTableName($tables);
        $this->buildRepository($repositorys);
    }

    /**
     * 设置目录/命名空间.
     *
     * @param array $dirInfo
     */
    private function setDirInfo(array $dirInfo): void
    {
        $this->repositoryDir = $dirInfo['path'] ?? '';
        $this->repositoryNamespace = $dirInfo['namespace'] ?? '';
    }

    private function buildRepository(array $repositorys): void
    {
        !is_dir($this->repositoryDir) && mkdir($this->repositoryDir, 0755, true);
        foreach ($repositorys as $repository) {
            $repositoryName = $repository . $this->extName;
            $this->buildRepositoryFile($repositoryName);
        }
    }

    private function buildRepositoryFile(string $repositoryName): void
    {
        $filePath = sprintf('%s/%s%s',
                $this->repositoryDir,
                $repositoryName,
                $this->fileExt
            );
        if (!file_exists($filePath)) {
            $fileCode = $this->getRepositoryFileCode($repositoryName);
            $fp = fopen($filePath, 'w');
            fwrite($fp, $fileCode);
        }
    }

    private function getRepositoryFileCode(string $repositoryName): string
    {
        $templates = $this->getTemplateFile('Base');
        $namespace = $this->repositoryNamespace;
        $useNamespace = [
            'Shadon\Mvc\User\Repository',
        ];
        $className = $repositoryName;
        $extendsName = 'Repository';
        $className = $this->getClassName($className, $extendsName, []);
        $namespace = $this->getNamespace($namespace);
        $useNamespace = $this->getUseNamespace($useNamespace);

        return sprintf($templates, $namespace, $useNamespace, $className, '', '');
    }
}