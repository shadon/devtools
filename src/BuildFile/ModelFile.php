<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\BuildFile;

use Phalcon\Db;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Symfony\Component\Console\Exception\RuntimeException;

/**
 * Model生成类.
 *
 * @author eellytools<localhost.shell@gmail.com>
 */
class ModelFile extends File
{
    /**
     * model目录.
     *
     * @var string
     */
    protected $modelDir = '';

    /**
     * model命名空间.
     *
     * @var string
     */
    protected $modelNamespace = '';

    /**
     * model后缀
     *
     * @var string
     */
    protected $extName = '';

    private $sqlDir = ROOT_PATH . '/config/sql/';

    private $moduleDir = '';

    private $sqlFile = '';

    private $dbName = '';

    private $logicDirInfo = [];

    /**
     * model构建.
     *
     * @param string $moduleName
     * @param string $dirInfo
     *
     * @return array
     */
    public function run(string $moduleName, array $tables = []): void
    {
        $this->setModuleName($moduleName);
        $this->setDb();
        $tables = $tables ?: $this->getTables();

        if (empty($tables)) {
            echo $this->moduleName.'模块的表不存在,生成Model失败'.PHP_EOL;
        } else {
            $this->config->module->beforeLogic && (new LogicFile($this->di))->run($this->moduleName, $this->logicDirInfo, $tables, true);
            $this->buildModel($tables);
        }
    }

    public function buildModel(array $tables)
    {
        foreach ($tables as $table) {
            $modelName = $this->getModelNameBytableName($table);
            $filePath = $this->modelDir.'/'.$modelName.$this->fileExt;
            !file_exists($filePath) && file_put_contents($filePath, $this->getModelFileCode($modelName, $this->getProperties($table)));
        }
    }

    /**
     * 设置模块目录信息.
     *
     * @param array $dirInfo
     */
    public function setDirInfo(array $dirInfo): void
    {
        $this->modelDir = $dirInfo['path'] ?? '';
        $this->modelNamespace = $dirInfo['namespace'] ?? '';
        $this->moduleDir = $dirInfo['moduleDir'] ?? '';
        $this->logicDirInfo = $dirInfo['logic'] ?? [];
    }

    /**
     * 获取模型文件code.
     *
     * @param string $modelName
     *
     * @return string
     */
    private function getModelFileCode(string $modelName, array $properties): string
    {
        $templates = $this->getTemplateFile('Base');
        $namespace = $this->getNamespace($this->modelNamespace);
        $useNamespace = $this->getUseNamespace(['Shadon\Mvc\Model']);
        $className = $this->getClassName($modelName, 'Model');
        $pk = '';
        foreach ($properties as $pVal) {
            if ('PRI' == $pVal['COLUMN_KEY']) {
                $pk = $pVal['COLUMN_NAME'];
            }
            $p[$pVal['COLUMN_NAME']] = [
                'type'      => 'general',
                'qualifier' => 'public',
                'tips'      => $this->getCommentary($pVal),
            ];
        }
        !empty($pk) && $p['pk'] = [
            'type'      => 'general',
            'qualifier' => 'protected',
            'value'     => $pk,
            'valueType' => 'string',
            'tips'      => $this->getPropertiesCommentary('主键', 'int'),
        ];
        $p['tableName'] = [
            'type'      => 'general',
            'qualifier' => 'protected',
            'value'     => $this->getTableNameByModelName($modelName),
            'valueType' => 'string',
            'tips'      => $this->getPropertiesCommentary('表名', 'string'),
        ];
        $fields = implode(',', array_column($properties, 'COLUMN_NAME'));
        $fieldsValue = '['.PHP_EOL.str_repeat(' ', 8)."'base' => '{$fields}',".PHP_EOL.str_repeat(' ', 4).']';
        $p['FIELD_SCOPE'] = [
            'type'      => 'const',
            'qualifier' => 'protected',
            'value'     => $fieldsValue,
            'tips'      => $this->getPropertiesCommentary('字段空间', 'array'),
        ];

        $properties = $this->getClassProperties($p);

        return sprintf($templates, $namespace, $useNamespace, $className, $properties, '');
    }

    /**
     * 获取模块表.
     *
     * @return array
     */
    public function getTables(): array
    {
        !is_dir($this->modelDir) && mkdir($this->modelDir, 0755, true);
        $isInit = true;
        $dirInfo = new \DirectoryIterator($this->modelDir);
        foreach ($dirInfo as $dir){
            !$dir->isDot() && $isInit = false;
        }
        $isInit && $this->initTable();
        $statement = $this->di->getDb()->query('SHOW TABLES');
        $tables = $statement->fetchAll(Db::FETCH_COLUMN);

        return $tables;
    }

    /**
     * 获取表字段.
     *
     * @param string $dbName
     * @param string $tableName
     *
     * @return array
     */
    private function getProperties(string $tableName): array
    {
        $statement = $this->di->getDb()->query("SELECT COLUMN_NAME,DATA_TYPE,COLUMN_KEY,COLUMN_COMMENT FROM
            information_schema. COLUMNS WHERE table_schema = '{$this->dbName}' AND table_name = '{$tableName}';");
        $properties = $statement->fetchAll(Db::FETCH_ASSOC);

        return $properties;
    }

    /**
     * 字段注释.
     *
     * @param array $properties
     *
     * @return string
     */
    private function getCommentary(array $properties): string
    {
        $type = 'unknown';
        if (preg_match('/char|text/', $properties['DATA_TYPE'])) {
            $type = 'string';
        } elseif (preg_match('/int/', $properties['DATA_TYPE'])) {
            $type = 'int';
        } elseif (preg_match('/decimal|float|double/', $properties['DATA_TYPE'])) {
            $type = 'float';
        }

        return $this->getPropertiesCommentary($properties['COLUMN_COMMENT'], $type);
    }

    /**
     * 通过表名获取模块名.
     *
     * @param string $tableName
     *
     * @return string
     */
    private function getModelNameBytableName(string $tableName): string
    {
        $modelName = '';
        if (false !== strpos($tableName, '_')) {
            $tableArr = explode('_', $tableName);
            $modelName = array_reduce($tableArr, function ($str, $val) {
                return $str .= ucfirst($val);
            });
        } else {
            $modelName = ucfirst($tableName);
        }

        return $modelName.$this->extName;
    }

    /**
     * 通过模型名获取表名.
     *
     * @param string $modelName
     *
     * @return string
     */
    private function getTableNameByModelName(string $modelName): string
    {
        $tableName = preg_replace_callback('/([A-Z]{1})/', function ($matches) {
            return '_'.strtolower($matches[0]);
        }, strtr($modelName, [$this->extName => '']));

        return ltrim($tableName, '_');
    }

    public function setDb(string $moduleName = '')
    {
        $moduleName = !empty($moduleName) ? $moduleName : $this->moduleName;
        $config = $this->config;
        $this->dbName = $config->module->dbPrefix ? $config->module->dbPrefix.strtolower($moduleName) : strtolower($moduleName);
        $this->sqlFile = $this->sqlDir . $this->dbName . '.sql';
        try {
            $db = new Mysql([
                'host' => $config->module->dbHost,
                'username' => $config->module->dbUser,
                'password' => $config->module->dbPass,
                'dbname' => $this->dbName,
                'port' => $config->module->dbPort,
                'charset' => $config->module->dbCharset
            ]);
            $this->di->setShared('db', $db);
        } catch (\PDOException $e) {
            if (1049 === (int) $e->getCode() && file_exists($this->sqlFile)) {
                $this->initDatabase();
            }else {
                $outMessage = sprintf('%s生成Model失败,%s,检查数据库配置或%s文件是否配置正确' . PHP_EOL,
                    $this->dbName,
                    $e->getMessage(),
                    $this->sqlDir
                    );
                echo $outMessage;
                askToUnlinkDir('是否清除生成的文件(错误配置会导致生成脏文件)，请慎重选择：[Y/N]' . PHP_EOL, $this->moduleDir);
                exit();
            }
        }
    }

    private function initDatabase(): void
    {
        throwIf(empty($this->dbName), RuntimeException::class, 'The database name cannot be empty');

        $config = $this->config;
        $initDb = new Mysql([
            'host' => $config->module->dbHost,
            'username' => $config->module->dbUser,
            'password' => $config->module->dbPass,
            'port' => $config->module->dbPort,
            'charset' => $config->module->dbCharset
        ]);
        $initDb->execute('CREATE DATABASE IF NOT EXISTS ' . $this->dbName . ' DEFAULT CHARACTER SET = utf8');
        $initDb->execute('USE ' . $this->dbName);
        $this->di->setShared('db', $initDb);
    }

    private function initTable(): void
    {
        if (!file_exists($this->sqlFile)){
            echo $this->sqlFile . ' not exist' . PHP_EOL;
            $message = '是否清除生成的文件(错误的参数会导致生成脏文件)，请慎重选择：[Y/N]' . PHP_EOL;
            askToUnlinkDir($message, $this->moduleDir);
            exit;
        }

        $sqlContent = file_get_contents($this->sqlFile);
        $this->di->getDb()->execute($sqlContent);
    }
}
