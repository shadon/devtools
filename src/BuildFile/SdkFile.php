<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\BuildFile;

class SdkFile extends File
{
    protected $sdkDir = '';

    protected $sdkNamespace = '';

    protected $extName = 'Interface';

    public function run(string $moduleName, array $dirInfo, array $tables): void
    {
        $this->setModuleName($moduleName);
        $this->setDirInfo($dirInfo);
        $interfaces = $this->convertTableName($tables);
        array_walk($interfaces, function(&$interface) use ($moduleName){
            ucfirst($moduleName) !== $interface && $interface = strtr($interface, [ucfirst($moduleName) => '']);
        });
        $this->buildInterface($interfaces);
    }

    /**
     * 设置目录/命名空间.
     *
     * @param array $dirInfo
     */
    private function setDirInfo(array $dirInfo): void
    {
        $this->sdkDir = $dirInfo['path'] ?? '';
        $this->sdkNamespace = $dirInfo['namespace'] ?? '';
    }

    /**
     * 生成service内的interface文件
     */
    private function buildInterface(array $interfaces): void
    {
        !is_dir($this->sdkDir) && mkdir($this->sdkDir, 0755, true);
        $templates = $this->getTemplateFile('Base');
        foreach ($interfaces as $interface){
            $className = $interface . $this->extName;
            $classPath = sprintf('%s/%s%s',
                    $this->sdkDir,
                    $className,
                    $this->fileExt
                );
            if(!file_exists($classPath)){
                $dtoNamespace = sprintf('%s\\SDK\\%s\\DTO\\%sDTO',
                        getenv('PROJECT_NAMESPACE'),
                        ucfirst($this->moduleName),
                        $interface
                    );
                $useNamespace = $this->getUseNamespace([$dtoNamespace]);
                $classBody = $this->getInterfaceInitializeCode($interface);
                $namespace = $this->sdkNamespace;
                $className = $this->getInterfaceName($className);
                $namespace = $this->getNamespace($namespace);

                file_put_contents($classPath, sprintf($templates, $namespace, $useNamespace, $className, '', $classBody));
            }
        }
    }

    private function getInterfaceInitializeCode(string $interfaceName): string
    {
        $getMethod = [
            'qualifier' => 'public',
            'params' => [
                ['type' => 'int', 'name' => '{InterfaceName}Id'],
            ],
            'return' => [
                'type' => '{InterfaceName}DTO',
            ],
        ];
        $addMethod = [
            'qualifier' => 'public',
            'params' => [
                ['type' => 'array', 'name' => 'data'],
            ],
            'return' => [
                'type' => 'bool',
            ],
        ];
        $updateMethod = [
            'qualifier' => 'public',
            'params' => [
                ['type' => 'int', 'name' => '{InterfaceName}Id'],
                ['type' => 'array', 'name' => 'data'],
            ],
            'return' => [
                'type' => 'bool',
            ],
        ];
        $deleteMethod = [
            'qualifier' => 'public',
            'params' => [
                ['type' => 'int', 'name' => '{InterfaceName}Id'],
            ],
            'return' => [
                'type' => 'bool',
            ],
        ];
        $listPageMethod = [
            'qualifier' => 'public',
            'params' => [
                ['type' => 'array', 'name' => 'condition', 'defaultValue' => '[]'],
                ['type' => 'int', 'name' => 'currentPage', 'defaultValue' => '1'],
                ['type' => 'int', 'name' => 'limit', 'defaultValue' => '10'],
            ],
            'return' => [
                'type' => 'array',
            ],
        ];
        $methodArr = [
            'get{InterfaceName}' => $getMethod,
            'add{InterfaceName}' => $addMethod,
            'update{InterfaceName}' => $updateMethod,
            'delete{InterfaceName}' => $deleteMethod,
            'list{InterfaceName}Page' => $listPageMethod,
        ];

        $methodDescribe = <<<EOF
    /**
     *
     * @author eellytools<localhost.shell@gmail.com>
     */\n
EOF;
        $initializeCode = '';
        foreach($methodArr as $methodName => $methodInfo){
            $qualifier = $methodInfo['qualifier'];
            $returnType = str_replace('{InterfaceName}', $interfaceName, $methodInfo['return']['type']);
            $params = $this->getInterfaceInitializeParams($interfaceName, $methodInfo['params']);

            $initializeCode .= $methodDescribe;
            $methodName = str_replace('{InterfaceName}', $interfaceName, $methodName);
            $methodStr = <<<EOF
    $qualifier function $methodName($params): $returnType;\n\n
EOF;
            $initializeCode .= $methodStr;
        }

        return $initializeCode;
    }

    private function getInterfaceInitializeParams(string $interfaceName, array $params): string
    {
        if(empty($params)){
            return '';
        }
        $paramStr = '';
        foreach($params as $param){
            $paramStr .= $param['type'] . ' $' . str_replace('{InterfaceName}', lcfirst($interfaceName), $param['name']) . (isset($param['defaultValue']) ? ' = ' . $param['defaultValue'] : '') . ', ';
        }

        return rtrim($paramStr, ', ');
    }
}