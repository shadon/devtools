<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\BuildFile;

/**
 * Logic生成类.
 *
 * @author eellytools<localhost.shell@gmail.com>
 */
class LogicFile extends File
{
    /**
     * logic目录.
     *
     * @var string
     */
    protected $logicDir = '';

    /**
     * logic命名空间.
     *
     * @var string
     */
    protected $logicNamespace = '';

    /**
     * logic后缀名.
     *
     * @var string
     */
    protected $extName = 'Logic';

    protected $useNamespace = [];

    public function run(string $moduleName, array $dirInfo, array $tables, bool $beforeLogic = false): void
    {
        $this->setModuleName($moduleName);
        $this->setDirInfo($dirInfo, $beforeLogic);
        $logics = $this->convertTableName($tables);
        $this->buildLogic($logics, $beforeLogic);
    }

    /**
     * 设置目录/命名空间.
     *
     */
    private function setDirInfo(array $dirInfo, bool $beforeLogic): void
    {
        $this->logicDir = $dirInfo['path'] ?? '';
        $this->logicNamespace = $dirInfo['namespace'] ?? '';
        $this->serviceNamespace = $dirInfo['serviceNamespace'] ?? '';
        $this->serviceDir = $dirInfo['servicePath'] ?? '';
        if (!$beforeLogic){
            /** @var \Phalcon\Loader $loader */
            $loader = $this->loader;
            $loader->registerNamespaces([
                $this->serviceNamespace => $this->serviceDir,
            ]);
            $loader->register();
        }
    }

    /**
     * 生成logic.
     *
     * @param array $logics
     */
    private function buildLogic(array $logics, bool $beforeLogic): void
    {
        !is_dir($this->logicDir) && mkdir($this->logicDir, 0755, true);
        foreach ($logics as $logic) {
            $this->useNamespace = [];
            if ($beforeLogic){
                $this->buildLogicFile($logic);
            }else {
                $this->buildLogicFileByInterface($logic);
            }
        }
    }

    private function buildLogicFile(string $logic): void
    {
        $filePath = $this->logicDir.'/' . $logic.$this->extName . $this->fileExt;
        if (!file_exists($filePath)) {
            $fileCode = $this->getLogicFileCode($logic);
            $fp = fopen($filePath, 'w');
            fwrite($fp, $fileCode);
        }
    }

    private function getLogicFileCode(string $logic): string
    {
        $templates = $this->getTemplateFile('Base');
        $namespace = $this->logicNamespace;
        $implementName = ucfirst($this->moduleName) . $logic . 'Interface';
        $useNamespace = [
            'Shadon\\Mvc\\LogicController',
            getenv('PROJECT_SDK_NAMESPACE') . '\\SDK\\' . ucfirst($this->moduleName) . '\\Service\\' . $implementName,
        ];
        $this->setUseNamespace($useNamespace);
        $className = $logic . $this->extName;
        $extendsName = 'LogicController';
        $className = $this->getClassName($className, $extendsName, [$implementName]);
        $namespace = $this->getNamespace($namespace);
        $useNamespace = $this->getUseNamespace($this->useNamespace);

        return sprintf($templates, $namespace, $useNamespace, $className, '', '');
    }

    private function buildLogicFileByInterface(string $interface): void
    {
        $className = strtr($interface, ['Interface' => '']);
        ucfirst($this->moduleName) !== $className && $className = strtr($className, [ucfirst($this->moduleName) => '']);
        $className .= $this->extName;
        $classPath = sprintf('%s/%s%s',
                $this->logicDir,
                $className,
                $this->fileExt
            );
        !file_exists($classPath) && file_put_contents($classPath, $this->getLogicFileCodeByInterface($className, $interface));
    }

    private function getLogicFileCodeByInterface(string $className, string $interface): string
    {
        $templates = $this->getTemplateFile('Base');
        $interfaceName = sprintf('%s\\SDK\\%s\\Service\\%s',
                getenv('PROJECT_SDK_NAMESPACE'),
                ucfirst($this->moduleName),
                $interface
            );
        $this->setUseNamespace([$interfaceName, 'Shadon\\Mvc\\LogicController']);
        $interfaceReflection = new \ReflectionClass($interfaceName);
        $methods = $interfaceReflection->getMethods();
        $classBody = $this->getLogicMethodCode($methods);
        $namespace = $this->getNamespace($this->logicNamespace);
        $useNamespace = $this->getUseNamespace($this->useNamespace);
        $className = $this->getClassName($className, 'LogicController', [$interface]);

        return sprintf($templates, $namespace, $useNamespace, $className, '', $classBody);
    }

    private function setUseNamespace($namespace): void
    {
        if (is_string($namespace)) {
            !in_array($namespace, $this->useNamespace) && $this->useNamespace[] = $namespace;
        } elseif (is_array($namespace)) {
            $this->useNamespace = array_unique(array_merge($this->useNamespace, $namespace));
        }
    }

    private function getLogicMethodCode(array $methods): string
    {
        $methodBuild = [];
        foreach ($methods as $method) {
            $methodName = $method->getName();
            $methodModifier = \Reflection::getModifierNames($method->getModifiers())[1];
            $methodDoc = $method->getDocComment();
            $methodParam = $this->getMethodParams($method->getParameters());
            $methodReturn = ($method->getReturnType() instanceof \ReflectionType) ? $method->getReturnType()->getName() : '';

            $commonInfo = [
                'modifier' => $methodModifier,
                'document' => $methodDoc,
                'params' => $methodParam,
                'return' => $methodReturn,
            ];
            $syncMethod = array_merge($commonInfo, [
                'name' => $methodName,
                'sync' => true,
            ]);
            array_push($methodBuild, $syncMethod);
        }

        return $this->getMethodCode($methodBuild);
    }

    /**
     * 获取方法参数.
     *
     * @param array $params
     *
     * @return array
     */
    private function getMethodParams(array $params): array
    {
        $methodParams = [];
        foreach ($params as $param) {
            $methodParams[] = [
                'name'          => $param->getName(),
                'position'      => $param->getPosition(),
                'type'          => ($param->getType() instanceof \ReflectionNamedType) ? $param->getType()->getName() : '',
                'hasDefaultVal' => $param->isDefaultValueAvailable(),
                'defaultVal'    => $param->isDefaultValueAvailable() ? $param->getDefaultValue() : '',
            ];
        }

        return $methodParams;
    }

    private function getMethodCode(array $methods): string
    {
        $str = '';
        foreach ($methods as $method) {
            $paramCode = $this->getMethodParamCode($method['params']);
            $returnType = !empty($method['return']) ? ': ' . $this->checkParamType($method['return']) : '';
            $methodBody = '';
            $str .= <<<EOF
    {$method['document']}
    {$method['modifier']} function {$method['name']}{$paramCode}{$returnType}
    {
        {$methodBody}
    }\n\n
EOF;
        }

        return $str;
    }

    private function getMethodParamCode(array $params): string
    {
        $str = '(';
        foreach ($params as $param) {
            !empty($param['type']) && $str .= $this->checkParamType($param['type']).' ';
            $str .= '$'.$param['name'];
            $param['hasDefaultVal'] && $str .= ' = '. (is_array($param['defaultVal']) ? $this->arrayConvertsString($param['defaultVal']): $this->valueConvertsString($param['defaultVal']));
            $str .= ', ';
        }

        return rtrim($str, ', ').')';
    }

    private function checkParamType(string $paramType): string
    {
        if (false !== strpos($paramType, '\\')) {
            $this->setUseNamespace([$paramType]);
            $offset = strrpos($paramType, '\\');
            $paramType = substr($paramType, $offset + 1);
        }

        return $paramType;
    }
}
