<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\BuildFile;

use Phalcon\DiInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Api生成类.
 *
 * @author eellytools<localhost.shell@gmail.com>
 */
class ApiFile extends File
{
    /**
     * sdk目录.
     *
     * @var string
     */
    protected $sdkDir;

    /**
     * service目录.
     *
     * @var string
     */
    protected $serviceDir = '';

    /**
     * api目录.
     *
     * @var string
     */
    protected $apiDir = '';

    /**
     * sdk命名空间.
     *
     * @var string
     */
    protected $sdkNamespace = '';

    /**
     * service目录.
     *
     * @var string
     */
    protected $serviceNamespace = '';

    /**
     * api目录.
     *
     * @var string
     */
    protected $apiNamespace = '';

    /**
     * 导入的命名空间.
     *
     * @var array
     */
    protected $useNamespace = [];

    /**
     * api名称.
     *
     * @var string
     */
    protected $apiName = '';

    /**
     * @param DiInterface $di
     */
    public function __construct(DiInterface $di)
    {
        parent::__construct($di);
    }

    /**
     * api构建.
     *
     * @param string $moduleName
     * @param array  $logicDirInfo
     */
    public function run(string $moduleName): void
    {
        $this->setModuleName($moduleName);
        $this->setDirInfo();
        $this->buildApiFile();
    }

    /**
     * 设置目录信息.
     *
     * @return self
     */
    private function setDirInfo(): void
    {
        $ucModuleName = ucfirst($this->moduleName);
        $sdkPath = getenv('PROJECT_SDK_PATH');
        $projectSdkNamespace = getenv('PROJECT_SDK_NAMESPACE');
        $this->sdkDir = sprintf('%s/src/SDK/%s',
                $sdkPath,
                $ucModuleName
            );
        $this->serviceDir = $this->sdkDir.'/Service';
        $this->apiDir = $this->sdkDir.'/Api';
        $this->sdkNamespace = $projectSdkNamespace . '\\SDK\\';
        $this->serviceNamespace = $this->sdkNamespace . $ucModuleName . '\\Service\\';
        $this->apiNamespace = $this->sdkNamespace . $ucModuleName . '\\Api';

        /** @var \Phalcon\Loader $loader */
        $loader = $this->di->get('loader');
        $loader->registerNamespaces([
            rtrim($this->serviceNamespace, '\\') => $this->serviceDir,
        ]);
        $loader->register();
    }

    /**
     * 设置api导入的命名空间.
     *
     * @param array $namespace
     */
    private function setUseNamespace($namespace): void
    {
        if (is_string($namespace)) {
            !in_array($namespace, $this->useNamespace) && $this->useNamespace[] = $namespace;
        } elseif (is_array($namespace)) {
            $this->useNamespace = array_unique(array_merge($this->useNamespace, $namespace));
        }
    }

    /**
     * 生成api文件.
     *
     * @return string
     */
    private function buildApiFile(): void
    {
        if (!file_exists($this->serviceDir)){
            $this->createServiceDir();
        }

        !is_dir($this->apiDir) && mkdir($this->apiDir, 0755, true);
        $dirInfo = new \DirectoryIterator($this->serviceDir);
        $templates = $this->getTemplateFile('Base');
        foreach ($dirInfo as $file) {
            if (!$file->isDot() && $file->isFile()) {
                $apiImplements = strstr($file->getFilename(), '.', true);
                $this->apiName = strtr($apiImplements, ['Interface' => '']);
                // ucfirst($this->moduleName) !== $this->apiName && $this->apiName = strtr($this->apiName, [ucfirst($this->moduleName) => '']);
                $apiPath = $this->apiDir.'/'.$this->apiName.$this->fileExt;
                !is_file($apiPath) && file_put_contents($apiPath, $this->getApiFileCode($apiImplements, $templates));
            }
        }
    }

    private function createServiceDir(): void
    {
        $command = $this->application->find('sdk');
        $arguments = [
            'command' => 'sdk',
            '--module' => $this->moduleName,
        ];
        $input = new ArrayInput($arguments);
        $command->run($input, new ConsoleOutput());
    }

    /**
     * 获取api文件code.
     *
     * @param string $apiImplements
     * @param string $templates
     * @param bool $isLogic
     * @param bool $isCliApi
     *
     * @return string
     */
    private function getApiFileCode(string $apiImplements, string $templates, bool $isLogic = false, bool $isCliApi = false): string
    {
        $this->initUseNamespace('Api');
        $interfaceName = $this->serviceNamespace . $apiImplements;
        $this->setUseNamespace($interfaceName);
        try{
            $interfaceReflection = new \ReflectionClass($interfaceName);
        }catch (\ReflectionException $e){
            exit($e->getMessage() . '，请确保Service内的Interface存在' . PHP_EOL);
        }

        $methods = $interfaceReflection->getMethods();
        // 获取api需生成的方法code
        $classBody = $this->getApiMethodCode($methods, $isLogic, $isCliApi);
        $namespace = $this->getNamespace($this->apiNamespace);
        $useNamespace = $this->getUseNamespace($this->useNamespace);
        $className = $this->getClassName($this->apiName, '', [$apiImplements]);

        return sprintf($templates, $namespace, $useNamespace, $className, '', $classBody);
    }

    /**
     * 获取api方法code.
     *
     * @param array $methods
     * @param boole $isLogic
     * @param bool $isCliApi
     *
     * @return string
     */
    private function getApiMethodCode(array $methods, bool $isLogic = false, bool $isCliApi = false): string
    {
        $methodBuild = [];
        foreach ($methods as $method) {
            $methodName = $method->getName();
            $methodModifier = \Reflection::getModifierNames($method->getModifiers())[1];
            $methodDoc = $method->getDocComment();
            $methodParam = $this->getMethodParams($method->getParameters());
            $methodReturn = ($method->getReturnType() instanceof \ReflectionType) ? $method->getReturnType()->getName() : '';

            $commonInfo = [
                'modifier' => $methodModifier,
                'document' => $methodDoc,
                'params' => $methodParam,
                'return' => $methodReturn,
            ];
            $syncMethod = array_merge($commonInfo, [
                'name' => $methodName,
                'sync' => true,
            ]);
            $asyncMethod = array_merge($commonInfo, [
                'name' => $methodName . 'Async',
                'sync' => false,
                'return' => '',
            ]);
            array_push($methodBuild, $syncMethod);
            array_push($methodBuild, $asyncMethod);
        }

        return $this->getMethodCode($methodBuild, $isLogic, $isCliApi);
    }

    /**
     * 获取方法参数.
     *
     * @param array $params
     *
     * @return array
     */
    private function getMethodParams(array $params): array
    {
        $methodParams = [];
        foreach ($params as $param) {
            $methodParams[] = [
                'name'          => $param->getName(),
                'position'      => $param->getPosition(),
                'type'          => ($param->getType() instanceof \ReflectionNamedType) ? $param->getType()->getName() : '',
                'hasDefaultVal' => $param->isDefaultValueAvailable(),
                'defaultVal'    => $param->isDefaultValueAvailable() ? $param->getDefaultValue() : '',
            ];
        }

        return $methodParams;
    }

    /**
     * 获取方法code.
     *
     * @param array $methods
     * @param boole $isLogic
     * @param bool $isCliApi
     *
     * @return string
     */
    private function getMethodCode(array $methods, bool $isLogic = false, bool $isCliApi = false): string
    {
        $str = '';
        foreach ($methods as $method) {
            $paramCode = $this->getMethodParamCode($method['params']);
            $returnType = !empty($method['return']) ? ': ' . $this->checkParamType($method['return']) : '';
            $methodBody = $isLogic ? '' : $this->getMethodBodyCode($method['name'], $method['params'], $method['sync']);
            $str .= <<<EOF
    {$method['document']}
    {$method['modifier']} function {$method['name']}{$paramCode}{$returnType}
    {
        {$methodBody}
    }\n\n
EOF;
        }
        !$isLogic && $str .= $this->getInstanceCode();
        return $str;
    }

    /**
     * 获取方法参数的code.
     *
     * @param array $params
     *
     * @return string
     */
    private function getMethodParamCode(array $params): string
    {
        $str = '(';
        foreach ($params as $param) {
            !empty($param['type']) && $str .= $this->checkParamType($param['type']).' ';
            $str .= '$'.$param['name'];
            $param['hasDefaultVal'] && $str .= ' = '. (is_array($param['defaultVal']) ? $this->arrayConvertsString($param['defaultVal']): $this->valueConvertsString($param['defaultVal']));
            $str .= ', ';
        }

        return rtrim($str, ', ').')';
    }

    /**
     * 获取方法体code.
     *
     * @param string $methodName
     * @param array  $params
     *
     * @return string
     */
    private function getMethodBodyCode(string $methodName, array $params, bool $sync): string
    {
        false !== strrpos($methodName, 'Async') && $methodName = strtr($methodName, ['Async' => '']);
        $uri = strtolower($this->moduleName).'/'.lcfirst($this->apiName);
        $args = '';
        if (!empty($params)) {
            $args = ', '.array_reduce($params, function ($str, $val) {
                return $str .= '$'.$val['name'].', ';
            });
            $args = rtrim((string) $args, ', ');
        }
        $sync = $this->valueConvertsString($sync);

        return <<<EOF
return EellyClient::request('{$uri}', '{$methodName}', {$sync}{$args});
EOF;
    }

    /**
     * 验证参数类型.
     *
     * @param string $paramType
     *
     * @return string
     */
    private function checkParamType(string $paramType): string
    {
        if (false !== strpos($paramType, '\\')) {
            $this->setUseNamespace([$paramType]);
            $offset = strrpos($paramType, '\\');
            $paramType = substr($paramType, $offset + 1);
        }

        return $paramType;
    }

    /**
     * 初始化导入的命名空间.
     *
     * @param string $type
     */
    private function initUseNamespace(string $type): void
    {
        $this->useNamespace = 'Api' === $type ? ['Eelly\SDK\EellyClient'] : [];
    }

}
