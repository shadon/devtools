<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\BuildFile;

class ValidationFile extends File
{
    protected $validationDir = '';

    protected $validationNamespace = '';

    protected $extName = 'Validation';

    public function run(string $moduleName, array $dirInfo, array $tables): void
    {
        $this->setModuleName($moduleName);
        $this->setDirInfo($dirInfo);
        $validations = $this->convertTableName($tables);
        $this->buildValidation($validations);
    }

    /**
     * 设置目录/命名空间.
     *
     * @param array $dirInfo
     */
    private function setDirInfo(array $dirInfo): void
    {
        $this->validationDir = $dirInfo['path'] ?? '';
        $this->validationNamespace = $dirInfo['namespace'] ?? '';
    }

    private function buildValidation(array $validations): void
    {
        !is_dir($this->validationDir) && mkdir($this->validationDir, 0755, true);
        foreach ($validations as $validation) {
            $validationName = $validation . $this->extName;
            $this->buildValidationFile($validationName);
        }
    }

    private function buildValidationFile(string $validationName): void
    {
        $filePath = sprintf('%s/%s%s',
            $this->validationDir,
            $validationName,
            $this->fileExt
            );
        if (!file_exists($filePath)) {
            $fileCode = $this->getValidationFileCode($validationName);
            $fp = fopen($filePath, 'w');
            fwrite($fp, $fileCode);
        }
    }

    private function getValidationFileCode(string $validationName): string
    {
        $templates = $this->getTemplateFile('Base');
        $namespace = $this->validationNamespace;
        $className = $validationName;
        $extendsName = 'CommonValidation';
        $className = $this->getClassName($className, $extendsName, []);
        $namespace = $this->getNamespace($namespace);

        return sprintf($templates, $namespace, '', $className, '', '');
    }
}