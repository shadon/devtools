<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\ArrayInput;

class ModuleAclCommand extends BaseCommand
 {
     protected $canRunProjectType = [
         'api',
     ];

     protected $registerCheckMethods = [
         'checkProjectType',
         'checkProjectSdkPath',
     ];

     protected $enableEnv = true;

     private $moduleName = '';

     private $clientSecret = '><!@_@!-v-';

     /**
      * 接口服务名称
      *
      * @var string
      */
     private $serviceName = '{moduleName}\\Logic\\{serviceName}Logic';

     private $serviceDir = '';

     protected function configure()
     {
         $helpStr = 'module-acl --module=user';
         $this->setDescription('新增/更新模块的acl信息')
            ->setHelp($helpStr)
            ->addOption('module', null, InputOption::VALUE_OPTIONAL, '模块名');
     }

     protected function execute(InputInterface $input, OutputInterface $output)
     {
         /** @var \Phalcon\Config $config */
         $config = $this->dependencyInjector->getConfig();
         $modules = $input->getOption('module') ? [$input->getOption('module')] : $config->module->buildModules->toArray();
         $this->initAcl();
         foreach($modules as $module){
             $this->moduleName = $module;
             $this->setDir();
             if (!file_exists($this->serviceDir)){
                 $this->consoleOutput->error($this->serviceDir . ' does not exist');
                 continue;
             }
             $this->addModuleService();
             $this->consoleOutput->success('The ' . $module . ' module\'s Acl update successful');
         }

     }

     private function setDir()
     {
         if (!empty($this->moduleName)){
             $this->serviceDir = sprintf('%s/src/SDK/%s/Service',
                    $this->projectSdkPath,
                    ucfirst($this->moduleName)
                 );
         }
     }

     /**
      * 添加模块服务信息
      *
      * @param string $moduleName
      * @throws \Symfony\Component\Console\Exception\RuntimeException
      * @author wangjiang<wangjiang@eelly.net>
      * @since 2017年8月25日
      */
     private function addModuleService(): void
     {
         $clientData = [
             'clientKey' => $this->moduleName . 'module',
             'clientSecret' => password_hash($this->moduleName . $this->clientSecret, PASSWORD_BCRYPT),
             'orgName' => 'eellyapi',
         ];
         $this->addModuleAndClient($this->moduleName, $clientData);
         try {
             $dirInfo = new \DirectoryIterator($this->serviceDir);
         }catch (\UnexpectedValueException $e){
             $this->consoleOutput->error($this->serviceDir . ' does not exist');
             exit();
         }

         foreach ($dirInfo as $file) {
             if (!$file->isDot() && $file->isFile()) {
                 $apiImplements = strstr($file->getFilename(), '.', true);
                 $serviceName = strtr($apiImplements, ['Interface' => '']);
                 $searchArr = ['{moduleName}', '{serviceName}'];
                 $replaceArr = [ucfirst($this->moduleName), $serviceName];
                 $fullServiceName = str_replace($searchArr, $replaceArr, $this->serviceName);
                 $this->shadonAcl->addModuleService($fullServiceName, $this->moduleName);
                 $this->addPermission($this->moduleName, $serviceName);
             }
         }
     }

     /**
      * 添加模块和客户端信息
      *
      * @param string $moduleName
      * @author wangjiang<wangjiang@eelly.net>
      * @since 2017年8月25日
      */
     private function addModuleAndClient(string $moduleName, array $clientData = []): void
     {
         $this->shadonAcl->addModule($moduleName);
         $this->shadonAcl->addModuleClient($moduleName, $clientData);
         $this->shadonAcl->addRole($moduleName, null, $moduleName.'/*/*');
         $this->shadonAcl->addRoleClient($moduleName, $this->shadonAcl->getClientKeyByModuleName($moduleName));
     }

     /**
      * 添加Permission
      *
      * @param string $moduleName
      * @param string $serviceName
      * @param string $permissionName
      * @author wangjiang<wangjiang@eelly.net>
      * @since 2017年8月25日
      */
     private function addPermission(string $moduleName, string $serviceName): void
     {
         $command = $this->getApplication()->find('api-acl');
         $arguments = [
             'command' => 'api-acl',
             'module' => $moduleName,
             '--interface' => $serviceName,
         ];
         $input = new ArrayInput($arguments);
         $command->run($input, $this->consoleOutput);
     }
 }