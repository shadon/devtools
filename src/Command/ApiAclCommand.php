<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ApiAclCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectSdkNamespace',
        'checkProjectSdkPath',
    ];

    protected $enableEnv = true;

    protected $serviceDir = '';

    protected $serviceNamespace = '';

    protected $currentMethod = '';

    protected $moduleName = '';

    protected $serviceName = '';


    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'api-acl user --interface=info --method=getInfo';
        $this->setDescription('新增/更新接口acl信息')
        ->setHelp($help)
        ->addArgument('module', InputArgument::REQUIRED, '模块名')
        ->addOption('interface', null, InputOption::VALUE_OPTIONAL, '接口名')
        ->addOption('method', null, InputOption::VALUE_OPTIONAL, '方法名');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->moduleName = $input->getArgument('module');
        $this->setDir();
        $interfaceName = $input->getOption('interface') ? [$input->getOption('interface') . 'Interface'] : $this->getInterface();
        $methodName = $input->getOption('method');
        $this->initAcl();
        !empty($methodName) && $this->currentMethod = strtolower($methodName);
        foreach ($interfaceName as $interface){
            $this->addApi($interface);
            $this->consoleOutput->success($interface . ' Acl update successful');
        }
    }

    private function setDir()
    {
        $this->serviceDir = sprintf('%s/src/SDK/%s/Service',
                $this->projectSdkPath,
                ucfirst($this->moduleName)
            );
        $this->serviceNamespace = sprintf('%s\\SDK\\%s\\Service',
                $this->projectSdkNamespace,
                ucfirst($this->moduleName)
            );
        /** @var \Phalcon\Loader $loader */
        $loader = $this->dependencyInjector->get('loader');
        $loader->registerNamespaces([
            $this->serviceNamespace => $this->serviceDir,
        ]);
        $loader->register();
    }

    private function getInterface(): array
    {
        $implements = [];
        try {
            $dirInfo = new \DirectoryIterator($this->serviceDir);
        }catch (\UnexpectedValueException $e){
            $this->consoleOutput->error($this->serviceDir . ' does not exist');
            exit();
        }

        foreach ($dirInfo as $file) {
            if (! $file->isDot() && $file->isFile()) {
                $implements[] = strstr($file->getFilename(), '.', true);
            }
        }

        return $implements;
    }

    private function addApi(string $interfaceName)
    {
        $logicName = strtr($interfaceName, ['Interface' => '']);
        $this->serviceName = sprintf('%s\\Logic\\%sLogic',
                ucfirst($this->moduleName),
                $logicName
            );
        ucfirst($this->moduleName) !== $logicName && $logicName = strtr($logicName, [ucfirst($this->moduleName) => '']);
        $interfaceName = $this->serviceNamespace .'\\' . $interfaceName;
        try{
            $interfaceReflection = new \ReflectionClass($interfaceName);
        }catch (\ReflectionException $e){
            exit($e->getMessage() . '，请确保Service内的Interface存在' . PHP_EOL);
        }

        $methods = $interfaceReflection->getMethods();
        foreach ($methods as $method) {
            $methodName = $method->getName();
            $methodDoc = $method->getDocComment();
            $methodParam = $this->getMethodParams($method->getParameters());
            // 单独操作Permission时仅对当前操作的这个Permission进行ACL操作
            $isApi = ('' === $this->currentMethod ? true : $this->currentMethod === strtolower($methodName));
            if ($isApi) {
                $hashName = strtolower($this->moduleName) . '/' . lcfirst($logicName) . '/' . $methodName;
                $descriptions = !empty($methodDoc) ? $this->getMethodDescription($methodDoc) : [];
                $this->addPermission($hashName, $methodParam, $descriptions);
            }
        }
    }

    private function getMethodDescription(string $docComment): array
    {
        if (empty($docComment)) {
            return [];
        }
        // 方法名和方法描述
        $methodDescription = $paramDescription = $requestExample = $returnExample = [];
        preg_match('/[\/*\s]+([^@\n]*)[*\s]+([^@\n]*)/', $docComment, $description);
        $methodDescription['methodName'] = $description[1] ?? '';
        $methodDescription['methodDescribe'] = $description[2] ?? '';
        // 参数描述
        preg_match_all('/@param.*\$[\w]*\s+([^\s][^\n\*]*)\n/U', $docComment, $paramArr);
        $methodDescription['paramDescribe'] = $paramArr[1] ?? [];
        // 子参数的描述(参数为数组时)
        preg_match_all('/@param\s+(.*)\s+\$(.*)\[(.*)\]\s+([^\s][^\n\*]*)\n/U', $docComment, $subParamArr);
        $subParam = [];
        $subParamTypeArr = $subParamArr[1] ?? [];
        $parentParamArr = $subParamArr[2] ?? [];
        $subParamNameArr = $subParamArr[3] ?? [];
        $subParamdDescribeArr = $subParamArr[4] ?? [];
        array_walk($parentParamArr, function ($val, $key) use (&$subParam, $subParamTypeArr, $subParamNameArr, $subParamdDescribeArr) {
            $subParamName = $subParamNameArr[$key] ?? '';
            if (false !== strrpos($subParamName, '][')) {
                $nameArr = explode('][', $subParamName);
                $subParamName = array_pop($nameArr);
            }
            $subParam[$val][] = [
                'name' => trim($subParamName, '\'"'),
                'type' => $subParamTypeArr[$key] ?? '',
                'describe' => $subParamdDescribeArr[$key] ?? ''
            ];
        });
        $methodDescription['subParam'] = $subParam;
        // 请求参数示例
        preg_match('/@requestExample\((.*)\)/sU', $docComment, $requestExample);
        $methodDescription['requestExample'] = $requestExample[1] ?? '';
        $methodDescription['requestExample'] = preg_replace([
            "/\s\*\s/",
            "/\s/"
        ], '', $methodDescription['requestExample']);
        // 返回参数示例
        preg_match('/@returnExample\((.*)\)/sU', $docComment, $returnExample);
        $methodDescription['returnExample'] = $returnExample[1] ?? '';
        $methodDescription['returnExample'] = preg_replace([
            "/\s\*\s/",
            "/\s/"
        ], '', $methodDescription['returnExample']);
        // 返回参数和异常类型、描述
        preg_match_all('/@(return|throws)\s+([^\s]+)\s+([^\s\*]+[^\n]*)\s/', $docComment, $returnDescription);
        foreach ($returnDescription[1] as $key => $val) {
            $methodDescription['returnInfo'][] = [
                'type' => $val,
                'returnValue' => $returnDescription[2][$key] ?? '',
                'returnDescribe' => $returnDescription[3][$key] ?? ''
            ];
        }
        // 返回数据详情
        preg_match_all('/\*\s*([\w\[\]\'\"]+)\s*\|([\w]+)\s*\|(.*)/', $docComment, $returnDetailArr);
        $methodDescription['returnDetail'] = $this->getReturnDetail($returnDetailArr);

        return $methodDescription;
    }

    private function addPermission(string $hashName, array $methodParam, array $descriptions): void
    {
        $this->shadonAcl->addModuleService($this->serviceName, $this->moduleName);
        $permissionData = [];
        $isLogin = $this->checkPermissionIsLogin($methodParam);
        $permissionData = [
            'methodName' => $descriptions['methodName'] ?? '',
            'requestExample' => $descriptions['requestExample'] ?? '',
            'methodDescribe' => $descriptions['methodDescribe'] ?? '',
            'created_time' => time(),
            'isLogin' => (int)$isLogin,
        ];
        $this->shadonAcl->addPermission($hashName, $this->serviceName, $permissionData);
        $this->addPermissionRequest($hashName, $methodParam, $descriptions);
        $this->addPermissionReturn($hashName, $descriptions);
    }

    private function addPermissionRequest(string $hashName, array $methodParam, array $descriptions)
    {
        $requestExample = json_decode($descriptions['requestExample'], true) ?: [];
        $paramExample = is_array($requestExample) ? $this->getParamExample($methodParam, $requestExample) : [];
        $subParam = $descriptions['subParam'];
        $requestData = $subRequestData = [];
        if(!empty($methodParam)){
            foreach ($methodParam as $paramId => $param) {
                $requestData[] = [
                    'data_type' => 2,
                    'param_id' => $paramId,
                    'param_type' => $param['type'],
                    'param_name' => $param['name'],
                    'param_example' => $paramExample[$paramId] ?? '',
                    'comment' => $descriptions['paramDescribe'][$paramId] ?? '',
                    'is_must' => (int)! $param['hasDefaultVal'],
                    'created_time' => time()
                ];
                $subRequest = $subParam[$param['name']] ?? [];
                if(!empty($subRequest)){
                    foreach($subRequest as $sub){
                        $subParamIdMust = 0;
                        $subParamType = $sub['type'] ?? '';
                        false === stripos($subParamType, 'null') && $subParamIdMust = 1;
                        $subParamType = trim(trim($subParamType, 'nullNULL'),'|');
                        $subRequestData[$param['name']][] = [
                            'data_type' => 2,
                            'param_type' => $subParamType,
                            'param_name' => $sub['name'] ?? '',
                            'param_example' => $this->getSubParamExample($paramExample[$paramId] ?? '', $sub['name'] ?? ''),
                            'comment' => $sub['describe'] ?? '',
                            'is_must' => $subParamIdMust,
                            'created_time' => time()
                        ];
                    }
                }
            }

            $this->shadonAcl->addPermissionRequest($requestData, $hashName);
            $this->shadonAcl->addPermissionRequestSubParam($subRequestData, $hashName);
        }
    }

    private function addPermissionReturn(string $hashName, array $descriptions)
    {
        $returnData = [];
        $returnExample = $descriptions['returnExample'];
        if(isset($descriptions['returnInfo'])){
            $returnDetail = $descriptions['returnDetail'] ?? '';
            foreach ($descriptions['returnInfo'] as $info){
                $dataType = 'return' == $info['type'] ? 1 : 2;
                $returnData[] = [
                    'return_type' => $info['returnValue'],
                    'data_type' => $dataType,
                    'comment' => $info['returnDescribe'],
                    'return_example' => 1 == $dataType ? $returnExample : $info['returnValue'],
                    'created_time' => time(),
                    'return_detail' => 1 == $dataType ? $returnDetail : '',
                ];
            }
            $this->shadonAcl->addPermissionReturn($returnData, $hashName);
        }
    }

    /**
     * 获取参数的示例值
     *
     * @param array $methodParam
     * @param array $requestExample
     * @return array
     */
    private function getParamExample(array $methodParam, array $requestExample): array
    {
        if(empty($methodParam) || empty($requestExample)){
            return [];
        }
        $paramExample = [];
        foreach($methodParam as $param){
            $example = $requestExample[$param['position']] ?? ($requestExample[$param['name']] ?? '');
            is_array($example) && $example = json_encode($example, JSON_UNESCAPED_UNICODE);
            $paramExample[] = $example;
        }

        return $paramExample;
    }

    /**
     * 判断permission是否需要登录
     *
     * @param array $methodParam
     * @return bool
     */
    private function checkPermissionIsLogin(array $methodParam): bool
    {
        if(empty($methodParam)){
            return false;
        }
        $loginMark = [
            $this->projectSdkNamespace . '\\DTO\\UidDTO',
        ];
        $paramType = array_column($methodParam, 'type');

        return (bool)array_intersect($loginMark, $paramType);
    }

    /**
     * 获取子参数的示例
     *
     * @param string $parentParamExample
     * @param string $subName
     * @return string|string|mixed
     */
    private function getSubParamExample(string $parentParamExample, string $subName)
    {
        if(empty($parentParamExample) || empty($subName)){
            return '';
        }

        $isMultiple = 0 === strpos($parentParamExample, '[');
        $parentParamExample = json_decode($parentParamExample, true);
        $isMultiple && $parentParamExample = current($parentParamExample);

        return isset($parentParamExample[$subName]) ? json_encode($parentParamExample[$subName], JSON_UNESCAPED_UNICODE) : '';
    }

    /**
     * 获取返回数据详情
     *
     * @param array $returnDetailArr
     * @return string
     */
    private function getReturnDetail(array $returnDetailArr): string
    {
        $returnDetailNameArr = $returnDetailArr[1] ?? [];
        $returnDetailTypeArr = $returnDetailArr[2] ?? [];
        $returnDetailDescribeArr = $returnDetailArr[3] ?? [];
        $returnDetail = [];
        foreach($returnDetailNameArr as $key => $val){
            $nameArr = array_map(function($val){
                return str_replace([']', '"', "'"], '', $val);
            }, explode('[', $val));
                if(1 < count($nameArr)){
                    $currentName = array_pop($nameArr);
                    $detailData = [
                        'ret_name' => $currentName,
                        'data_type' => $returnDetailTypeArr[$key],
                        'remark' => $returnDetailDescribeArr[$key],
                        //                     'children' => [],
                    ];
                    $this->setReturnDetail($nameArr, $returnDetail, $detailData);
                }else{
                    $returnDetail[$val] = [
                        'ret_name' => $val,
                        'data_type' => $returnDetailTypeArr[$key],
                        'remark' => $returnDetailDescribeArr[$key],
                        //                     'children' => [],
                    ];
                }
        }

        return json_encode($returnDetail, JSON_UNESCAPED_UNICODE);
    }


    /**
     * 设置返回数据详情
     *
     * @param array $nameArr
     * @param array $returnDetail
     * @param array $detailData
     */
    private function setReturnDetail(array $nameArr, array &$returnDetail, array $detailData)
    {
        $name = array_shift($nameArr);
        if(0 == count($nameArr)){
            $returnDetail[$name]['children'][$detailData['ret_name']]  = $detailData;
            return;
        }

        $this->setReturnDetail($nameArr, $returnDetail[$name]['children'], $detailData);
    }


    /**
     * 获取方法参数.
     *
     * @param array $params
     *
     * @return array
     */
    private function getMethodParams(array $params): array
    {
        $methodParams = [];
        foreach ($params as $param) {
            $methodParams[] = [
                'name'          => $param->getName(),
                'position'      => $param->getPosition(),
                'type'          => ($param->getType() instanceof \ReflectionNamedType) ? $param->getType()->getName() : '',
                'hasDefaultVal' => $param->isDefaultValueAvailable(),
                'defaultVal'    => $param->isDefaultValueAvailable() ? $param->getDefaultValue() : '',
            ];
        }

        return $methodParams;
    }
}