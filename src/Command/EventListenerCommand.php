<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Shadon\DevTools\BuildFile\EventListenerFile;

class EventListenerCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectPath',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'event-listener user beforeAdd';
        $this->setDescription('EventListener层文件生成')
            ->setHelp($help)
            ->addArgument('module', InputArgument::REQUIRED, '模块名称')
            ->addArgument('name', InputArgument::REQUIRED, 'eventListener文件名');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('module');
        $eventListenerName  = [$input->getArgument('name')];
        $eventListenerFile = new EventListenerFile($this->getDI());
        $path = sprintf('%s/src/%s/EventListener',
            $this->projectPath,
            ucfirst($moduleName)
            );
        $namespace = sprintf('%s\\EventListener',
            ucfirst($moduleName)
            );
        $dirInfo = [
            'path' => $path,
            'namespace' => $namespace,
        ];
        $eventListenerFile->run($moduleName, $dirInfo, $eventListenerName);

        $this->consoleOutput->success('The '. $moduleName  .' module\'s eventListener file generation is successful');
    }
}