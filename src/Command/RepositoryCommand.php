<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Shadon\DevTools\BuildFile\RepositoryFile;

class RepositoryCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectPath',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'repository user --name=info';
        $this->setDescription('Repository层文件生成')
            ->setHelp($help)
            ->addArgument('module', InputArgument::REQUIRED, '模块名')
            ->addOption('name', null, InputOption::VALUE_OPTIONAL, 'repository文件名');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('module');
        $repositoryName  = $input->getOption('name') ? [$input->getOption('name')] : $this->getModuleTables($moduleName);;
        $repositoryFile = new RepositoryFile($this->getDI());
        $path = sprintf('%s/src/%s/Repository',
            $this->projectPath,
            ucfirst($moduleName)
            );
        $namespace = sprintf('%s\\Repository',
            ucfirst($moduleName)
            );
        $dirInfo = [
            'path' => $path,
            'namespace' => $namespace,
        ];
        $repositoryFile->run($moduleName, $dirInfo, $repositoryName);

        $this->consoleOutput->success('The '. $moduleName  .' module\'s repository file generation is successful');
    }
}