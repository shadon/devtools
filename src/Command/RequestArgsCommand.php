<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Phalcon\Annotations\Adapter\Memory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Exception\RuntimeException;

class RequestArgsCommand extends BaseCommand
{
    private $annotations = [
        'example' => 'requestExample',
        'reqArgs' => 'reqArgs',
    ];

    private $parameterType = [
        301 => 'int',
        302 => 'double',
        303 => 'string',
        304 => 'null',
        305 => 'bool',
        306 => 'bool',
        308 => 'array'
    ];

    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectSdkPath',
        'checkProjectSdkNamespace',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setDescription('生成方法的请求参数说明')
            ->setHelp('request-args goods goods getStoreAddress')
            ->addArgument('module', InputArgument::REQUIRED, '模块名')
            ->addArgument('interface', InputArgument::REQUIRED, '接口名')
            ->addArgument('method',InputArgument::REQUIRED, '方法名');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('module');
        $interfaceName = $input->getArgument('interface');
        $methodName = $input->getArgument('method');
        $this->setMethodNewDocument($moduleName, $interfaceName, $methodName);
        $output->writeln('生成完成');
    }

    /**
     * 设置方法的请求参数说明文档
     *
     * @param string $moduleName
     * @param string $interfaceName
     * @param string $methodName
     * @throws RuntimeException
     */
    private function setMethodNewDocument(string $moduleName, string $interfaceName, string $methodName): void
    {
        $className = sprintf('%s\\SDK\\%s\\Service\\%sInterface',
                $this->projectSdkNamespace,
                ucfirst($moduleName),
                ucfirst($interfaceName)
            );
        $classPath = sprintf('%s/src/SDK/%s/Service/%sInterface.php',
                $this->projectSdkPath,
                ucfirst($moduleName),
                ucfirst($interfaceName)
            );
        throwIf(!file_exists($classPath), RuntimeException::class, $classPath . ' does not exist');

        $di = $this->getDI();
        /** @var \Phalcon\Loader $loader */
        $loader = $di->getShared('loader');
        $loader->registerClasses([
            $className => $classPath,
        ]);
        $loader->register();

        /** @var Memory $annotations */
        $annotations = $di->getShared(Memory::class);
        $reader = $annotations->getMethod($className, $methodName);
        if(!$reader->has($this->annotations['reqArgs'])){
            throw new RuntimeException('not found annotation:' . $this->annotations['reqArgs']);
        }
        $methodReflection = new \ReflectionMethod($className, $methodName);
        $filePath = $methodReflection->getFileName();
        $methodDoc = $methodReflection->getDocComment();
        $hasUidDTO = $this->hasUidDTO($methodReflection->getParameters());
        $requestArgs = $this->getMethodRequestExample($reader, $hasUidDTO) . sprintf('%-5s*', '');
        $newMethodDoc = preg_replace('/\*\s*@reqArgs/', $requestArgs, $methodDoc);
        $newFileContent = str_replace($methodDoc, $newMethodDoc, file_get_contents($filePath));
        file_put_contents($filePath, $newFileContent);
    }

    /**
     * 获取requestExample字符串
     *
     * @param string $moduleName 模块名
     * @param string $interfaceName 接口名
     * @param string $methodName 方法名称
     * @throws RuntimeException
     * @return string
     * @author wangjiang<wangjiang@eelly.net>
     * 2017年8月24日
     */
    private function getMethodRequestExample(\Phalcon\Annotations\Collection $reader, bool $hasUidDTO = false): string
    {
        if(!$reader->has($this->annotations['example'])){
            throw new RuntimeException('not found annotation:' . $this->annotations['example']);
        }

        /** @var \Phalcon\Annotations\Annotation $exAnnotation */
        $exAnnotation = $reader->get($this->annotations['example']);
        $requestExample = $exAnnotation->getArgument(0);
        if(!is_array($requestExample)){
            throw new RuntimeException('request example not json');
        }

        $arguments = $exAnnotation->getExprArguments();
        // 区分{}和[]json格式的数据
        $arguments = $arguments[0]['expr']['items'];
        $exprArguments = $this->getExprArguments($arguments);
        $requestArgsStr = '*' . PHP_EOL;
        foreach($exprArguments as $field => $fieldType){
            $field = explode('_', (string)$field);
            $field = array_reduce($field, function($str, $val){
                return $str .= empty($str) ? $val : '["'. $val .'"]';
            });
            $requestArgsStr .= sprintf('%-5s* @param %s %s', '', $fieldType, '$' . $field) . PHP_EOL;
        }
        $hasUidDTO && $requestArgsStr .= sprintf('%-5s* @param \\%s\\DTO\\UidDTO $user 登录用户信息', '', $this->projectNamespace) . PHP_EOL;


        return $requestArgsStr;
    }

    /**
     * 递归获取参数类型
     *
     * @param array $arguments
     * @param string $parentName
     * @return array
     */
    private function getExprArguments(array $arguments, string $parentName = ''): array
    {
        if(empty($arguments)){
            return [];
        }
        $exprs = [];
        foreach($arguments as $offset => $argument){
            if(!isset($argument['name'])){
                if (isset($argument['expr']['items'])) {
                    $items = $this->getExprArguments($argument['expr']['items'], $parentName . $offset . '_');
                } else {
                    $type = $argument['expr']['type'];
                    $items[$parentName . $offset] = $this->parameterType[$type] ?? '';
                }
                $exprs += $items;
                continue;
            }

            $name = $parentName . $argument['name'];
            $type = $argument['expr']['type'];
            $exprs[$name] = $this->parameterType[$type] ?? '';
            if(isset($argument['expr']['items'])){
                $args = $argument['expr']['items'];
                $exprs += $this->getExprArguments($args, $name . '_');
            }
        }

        return $exprs;
    }

    private function hasUidDTO(array $parameters): bool
    {
        if(empty($parameters)){
            return false;
        }

        foreach($parameters as $parameter){
            if($this->projectNamespace . '\\DTO\\UidDTO' === $parameter->getType()->getName()){
                return true;
            }
        }

        return false;
    }
}