<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class SetEnvCommand extends BaseCommand
{
    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'set-env name value';
        $this->setDescription('设置项目环境变量')
        ->setHelp($help)
        ->addArgument('name', InputArgument::REQUIRED, '环境变量名')
        ->addArgument('value', InputArgument::REQUIRED, '环境变量值');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $value = $input->getArgument('value');
        $setResult = $this->shadonEnv->setEnv($name, $value);

        $outMessage = $setResult ? 'The Setting success, ' . $name . '=' . $value : 'The Setting fail';
        $this->consoleOutput->text($outMessage);
    }
}