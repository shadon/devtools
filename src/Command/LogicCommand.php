<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Shadon\DevTools\BuildFile\LogicFile;

class LogicCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectPath',
        'checkProjectSdkPath',
        'checkProjectSdkNamespace',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'logic user --name=info';
        $this->setDescription('逻辑层文件生成')
            ->setHelp($help)
            ->addArgument('module', InputArgument::REQUIRED, '模块名')
            ->addOption('name', null, InputOption::VALUE_OPTIONAL, '逻辑文件名')
            ->addOption('beforeLogic', null, InputOption::VALUE_NONE, '提前根据模块表生成');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('module');
        $beforeLogic = $input->getOption('beforeLogic');
        $dirInfo = [];
        if ($beforeLogic){
            $logicName  = $this->getModuleTables($moduleName);
        }else if(!empty($logicName = $input->getOption('name'))){
            $beforeLogic = true;
            $logicName = [$logicName];
        }else {
            $projectSdkPath = $this->projectSdkPath;
            $projectSdkNamespace = $this->projectSdkNamespace;
            $ucModuleName = ucfirst($moduleName);
            $serviceDir = sprintf('%s/src/SDK/%s/Service',
                    $projectSdkPath,
                    $ucModuleName
                );
            $serviceNamespace = sprintf('%s\\SDK\\%s\\Service',
                    $projectSdkNamespace,
                    $ucModuleName
                );
            $dirInfo = [
                'servicePath' => $serviceDir,
                'serviceNamespace' => $serviceNamespace,
            ];
            $logicName = $this->getInterface($moduleName);
        }

        $logicFile = new LogicFile($this->getDI());
        $path = sprintf('%s/src/%s/Logic',
                $this->projectPath,
                ucfirst($moduleName)
            );
        $namespace = sprintf('%s\\Logic',
                ucfirst($moduleName)
            );
        $dirInfo += [
            'path' => $path,
            'namespace' => $namespace,
        ];
        $logicFile->run($moduleName, $dirInfo, $logicName, $beforeLogic);

        $this->consoleOutput->success('The '. $moduleName  .' module\'s logical file generation is successful');
    }

    private function getInterface(string $moduleName): array
    {
        $serviceDir = sprintf('%s/src/SDK/%s/Service',
                $this->projectSdkPath,
                ucfirst($moduleName)
            );
        $implements = [];
        try {
            $dirInfo = new \DirectoryIterator($serviceDir);
        }catch (\UnexpectedValueException $e){
            $this->consoleOutput->error($serviceDir . ' does not exist');
            exit();
        }
        foreach ($dirInfo as $file) {
            if (! $file->isDot() && $file->isFile()) {
                $implements[] = strstr($file->getFilename(), '.', true);
            }
        }

        return $implements;
    }
}