<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Shadon\DevTools\BuildFile\ApiFile;

class ApiCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectSdkNamespace',
        'checkProjectSdkPath',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'api user';
        $this->setDescription('API文件生成')
            ->setHelp($help)
            ->addArgument('module', InputArgument::REQUIRED, '模块名');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('module');
        $apiFile = new ApiFile($this->getDI());
        $apiFile->run($moduleName);
        $this->consoleOutput->success('The ' . $moduleName . ' module\'s api file generation is successful');
    }
}