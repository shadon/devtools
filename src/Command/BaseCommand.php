<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Command\Command;
use Phalcon\Di;
use Phalcon\DiInterface;
use Phalcon\Di\InjectionAwareInterface;
use Shadon\DevTools\Env;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Shadon\DevTools\BuildFile\ModelFile;
use Shadon\DevTools\Acl\Database;
use Phalcon\Db\Adapter\Pdo\Mysql;

class BaseCommand extends Command implements InjectionAwareInterface
{
    protected $envPath = '';

    protected $envFile = '';

    protected $enableEnv = false;

    protected $shadonEnv = null;

    /**
     * Dependency Injector
     *
     * @var \Phalcon\DiInterface
     */
    protected $dependencyInjector;

    protected $projectType = '';

    protected $projectPath = '';

    protected $projectNamespace = '';

    protected $projectSdkPath = '';

    protected $projectSdkNamespace = '';

    protected $consoleOutput = null;

    /**
     *
     * @var \Shadon\DevTools\Acl\Database
     */
    protected $shadonAcl = null;

    /**
     * Sets the dependency injector
     */
    public function setDI(DiInterface $dependencyInjector)
    {
        $this->dependencyInjector = $dependencyInjector;
    }

    /**
     * Returns the internal dependency injector
     */
    public function getDI()
    {
        if(!is_object($this->dependencyInjector)){
            $this->dependencyInjector = Di::getDefault();
        }

        return $this->dependencyInjector;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::initialize()
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->consoleOutput = new SymfonyStyle($input, $output);
        if ($this->enableEnv){
            $this->initEnv();
        }

        if (method_exists($this, 'commandInitialize')){
            call_user_func_array([$this, 'commandInitialize'], [$input, $output]);
        }

        $this->checkIsCanRun();
    }

    private function initEnv()
    {
        $this->shadonEnv = Env::getInstance($this->envPath, $this->envFile);
    }

    private function checkIsCanRun()
    {
        if (!empty($this->registerCheckMethods)){
            foreach ($this->registerCheckMethods as $method){
                throwIf(!method_exists($this, $method), RuntimeException::class, $method . ' not exists');
                call_user_func([$this, $method]);
            }
        }
    }

    private function checkProjectType()
    {
        $this->projectType = $this->shadonEnv->getEnv('PROJECT_TYPE');
        $message = '.env file does not exist PROJECT_TYPE, please check .env file';
        throwIf(empty($this->projectType), RuntimeException::class, $message);

        if (!empty($this->canRunProjectType)){
            $message = 'The current operation project type does not support running the ' . $this->getName() . ' command';
            throwIf(!in_array($this->projectType, $this->canRunProjectType), RuntimeException::class, $message);
        }
    }

    private function checkProjectPath()
    {
        $this->projectPath = rtrim($this->shadonEnv->getEnv('PROJECT_PATH'), DIRECTORY_SEPARATOR);
        $isThrow = false;
        $message = '';

        if (!$isThrow && empty($this->projectPath)){
            $isThrow = true;
            $message = '.env file does not exist PROJECT_PATH, please check .env file';
        }

        if (!$isThrow && !is_writeable($this->projectPath)){
            $isThrow = true;
            $message = 'The current operation project('.$this-> projectPath. ') does not exist or does not write permissions';
        }

        throwIf($isThrow, RuntimeException::class, $message);
    }

    private function checkProjectNamespace()
    {
        $this->projectNamespace = $this->shadonEnv->getEnv('PROJECT_NAMESPACE');
        if (empty($this->projectNamespace)){
            $question = '.env file does not exist PROJECT_NAMESPACE, please set it';
            $namespace = strtr($this->consoleOutput->ask($question),[' ' => '']);

            $command = $this->getApplication()->find('set-env');
            $arguments = array(
                'command' => 'set-env',
                'name' => 'PROJECT_NAMESPACE',
                'value' => $namespace,
            );
            $input = new ArrayInput($arguments);
            $res = $command->run($input, $this->consoleOutput);
            0 === $res && $this->projectNamespace = $namespace;
        }
    }

    private function checkProjectSdkNamespace()
    {
        $this->projectSdkNamespace = $this->shadonEnv->getEnv('PROJECT_SDK_NAMESPACE');
        if (empty($this->projectSdkNamespace)){
            $question = '.env file does not exist PROJECT_SDK_NAMESPACE, please set it';
            $namespace = strtr($this->consoleOutput->ask($question),[' ' => '']);

            $command = $this->getApplication()->find('set-env');
            $arguments = array(
                'command' => 'set-env',
                'name' => 'PROJECT_SDK_NAMESPACE',
                'value' => $namespace,
            );
            $input = new ArrayInput($arguments);
            $res = $command->run($input, $this->consoleOutput);
            0 === $res && $this->projectSdkNamespace = $namespace;
        }
    }

    private function checkProjectSdkPath()
    {
        $this->projectSdkPath = rtrim($this->shadonEnv->getEnv('PROJECT_SDK_PATH'), DIRECTORY_SEPARATOR);
        if (empty($this->projectSdkPath)){
            $question = '.env file does not exist PROJECT_SDK_PATH, please set it';
            $path = strtr($this->consoleOutput->ask($question),[' ' => '']);

            $command = $this->getApplication()->find('set-env');
            $arguments = array(
                'command' => 'set-env',
                'name' => 'PROJECT_SDK_PATH',
                'value' => $path,
            );
            $input = new ArrayInput($arguments);
            $res = $command->run($input, $this->consoleOutput);
            0 === $res && $this->projectSdkPath = $path;
        }
    }

    protected function getModuleTables(string $moduleName): array
    {
        $modelFile = new ModelFile($this->getDI());
        $modelFile->setDb($moduleName);
        $dirInfo = [
            'path' => sprintf('%s/src/%s/Model/Mysql',
                $this->projectPath,
                ucfirst($moduleName)
                )
        ];
        $modelFile->setDirInfo($dirInfo);
        $tables = $modelFile->getTables();

        return $tables;
    }

    /**
     * 初始化acl.
     */
    protected function initAcl(): void
    {
        $oauthDb = $this->dependencyInjector->get('config')->module->oauthDb->toArray();
        $this->dependencyInjector->setShared('oauthDb', function () use ($oauthDb) {
            return $db = new Mysql($oauthDb);
        });
        $this->dependencyInjector->setShared('shadonAcl', [
            'className' => Database::class,
            'properties' => [
                [
                    'name' => 'db',
                    'value' => [
                        'type' => 'service',
                        'name' => 'oauthDb'
                    ]
                ]
            ]
        ]);

        $this->shadonAcl = $this->dependencyInjector->get('shadonAcl');
    }
}