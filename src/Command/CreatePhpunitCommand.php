<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Shadon\DevTools\BuildFile\PhpunitFile;

class CreatePhpunitCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectPath',
        'checkProjectSdkNamespace',
        'checkProjectSdkPath',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $helpStr = <<<EOT
create-phpunit
create-phpunit moduleName
EOT;
        $this->setDescription('模块单元测试文件生成')
            ->setHelp($helpStr)
            ->addArgument('moduleName', InputArgument::OPTIONAL, '模块名');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('moduleName');
        $modules = isset($moduleName) ? [$moduleName] : $this->getDI()->getConfig()->module->buildModules;
        $repeatStr = str_repeat('=', 6);
        foreach ($modules as $module){
            $phpunitFile = new PhpunitFile($this->getDI());
            $logicPath = sprintf('%s/src/%s/Logic',
                    $this->projectPath,
                    ucfirst($module)
                );
            if (!file_exists($logicPath)){
                $this->consoleOutput->error($logicPath . ' directory does not exist');
                continue;
            }

            $phpunitPath = sprintf('%s/tests/api/src/%s/Logic',
                    $this->projectPath,
                    ucfirst($module)
                );
            $logicNamespace = sprintf('%s\\Logic',
                    ucfirst($module)
                );
            $dirInfo = [
                'logicPath' => $logicPath,
                'logicNamespace' => $logicNamespace,
                'phpunitPath' => $phpunitPath,
            ];
            $phpunitFile->run($module, $dirInfo);
            $this->consoleOutput->success('The '. $module .' module\'s phpunit file generation is successful');
        }
    }
}