<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitCommand extends BaseCommand
{
    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'init';
        $this->setDescription('初始化')
            ->setHelp($help);
     }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $setting = [];
        $setting['PROJECT_TYPE'] = $this->consoleOutput->ask('Setting project type', null, function($type){
            if (!in_array($type, ['api', 'application'])){
                $this->consoleOutput->error('Unsupported project types:' . $type);
                exit();
            }

            return $type;
        });
        $setting['PROJECT_PATH'] = $this->consoleOutput->ask('Setting project path');

        if ('api' === $setting['PROJECT_TYPE']){
            $setting['PROJECT_SDK_PATH'] = $this->consoleOutput->ask('Setting project sdk path');
            $setting['PROJECT_SDK_NAMESPACE'] = $this->consoleOutput->ask('Setting project sdk namespace');
            $this->shadonEnv->hasEnv('PROJECT_NAMESPACE') && $this->shadonEnv->delEnv('PROJECT_NAMESPACE');
        }elseif ('application' === $setting['PROJECT_TYPE']){
            $this->shadonEnv->hasEnv('PROJECT_SDK_PATH') && $this->shadonEnv->delEnv('PROJECT_SDK_PATH');
            $this->shadonEnv->hasEnv('PROJECT_SDK_NAMESPACE') && $this->shadonEnv->delEnv('PROJECT_SDK_NAMESPACE');
            $setting['PROJECT_NAMESPACE'] = $this->consoleOutput->ask('Setting project namespace');
        }

        $headers = [
            ['name', 'value'],
        ];
        foreach ($setting as $key => $value){
            $this->shadonEnv->setEnv($key, $value);
            $rows[] = [
                $key,
                $value
            ];
        }

        $this->consoleOutput->success('Setting success');
        $this->consoleOutput->table($headers, $rows);
    }
}
