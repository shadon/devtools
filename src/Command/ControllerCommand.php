<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Shadon\DevTools\BuildFile\ControllerFile;

class ControllerCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'application',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectPath',
        'checkProjectNamespace',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'controller user';
        $this->setDescription('Controller层文件生成')
            ->setHelp($help)
            ->addArgument('name', InputArgument::REQUIRED, '控制器名称');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $controller = $input->getArgument('name');
        $controllerFile = new ControllerFile($this->getDI());
        $path = sprintf('%s/src/Controller',
                $this->projectPath
            );
        $namespace = sprintf('%s\\Controller',
                $this->projectNamespace
            );
        $dirInfo = [
            'path' => $path,
            'namespace' => $namespace,
        ];
        $controllerFile->run($dirInfo, [$controller]);

        $this->consoleOutput->success('The '. $controller. ' controller file generation is successful');
    }
}