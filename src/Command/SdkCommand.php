<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Process;
use Shadon\DevTools\BuildFile\SdkFile;

class SdkCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectSdkPath',
        'checkProjectSdkNamespace',
    ];

    protected $enableEnv = true;

    private $initGitAddress = 'https://gitee.com/shadon/sdk-example.git';

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'sdk --module=user';
        $this->setDescription('SDK构建')
        ->setHelp($help)
        ->addOption('module', null, InputOption::VALUE_OPTIONAL, 'Module name')
        ->addOption('type', null, InputOption::VALUE_OPTIONAL, 'Operation type');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $operationType = $input->getOption('type');
        $moduleName = $input->getOption('module');

        if ('init' === $operationType){
            $this->initSdk();
            $this->consoleOutput->success('The ' . $moduleName . ' module\'s sdk initialize is successful');
        }elseif(!empty($moduleName)) {
            $sdkFile = new SdkFile($this->getDI());
            $tables = $this->getModuleTables($moduleName);
            $moduleName = ucfirst($moduleName);
            $sdkPath = sprintf('%s/src/SDK/%s/Service', $this->projectSdkPath, $moduleName);
            $sdkNamespace = sprintf('%s\\SDK\\%s\\Service', $this->projectSdkNamespace, $moduleName);
            $dirInfo = [
                'path' => $sdkPath,
                'namespace' => $sdkNamespace
            ];
            $sdkFile->run($moduleName, $dirInfo, $tables);

            $this->consoleOutput->success('The ' . $moduleName . ' module\'s sdk file generation is successful');
        }else {
            $this->consoleOutput->error(['command option cannot be empty', 'example: sdk --module=user']);
        }
    }

    private function initSdk(): void
    {
        $process = new Process($this->getCommandLine());
        $process->run();
        $gitPath = $this->projectSdkPath . '/.git';
        file_exists($gitPath) && unlinkDir($gitPath);
    }

    private function getCommandLine(): string
    {
        $commandLine = sprintf('git clone --depth=1 %s %s',
                $this->initGitAddress,
                $this->projectSdkPath
            );

        return $commandLine;
    }
}