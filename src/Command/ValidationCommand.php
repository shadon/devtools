<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Shadon\DevTools\BuildFile\ValidationFile;

class ValidationCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectPath',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'validation user --name=info';
        $this->setDescription('Validation层文件生成')
            ->setHelp($help)
            ->addArgument('module', InputArgument::REQUIRED, '模块名称')
            ->addOption('name', '', InputOption::VALUE_OPTIONAL, 'validation文件名');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('module');
        $repositoryName  = $input->getOption('name') ? [$input->getOption('name')] : $this->getModuleTables($moduleName);
        $validationFile = new ValidationFile($this->getDI());
        $path = sprintf('%s/src/%s/Validation',
            $this->projectPath,
            ucfirst($moduleName)
            );
        $namespace = sprintf('%s\\Validation',
            ucfirst($moduleName)
            );
        $dirInfo = [
            'path' => $path,
            'namespace' => $namespace,
        ];
        $validationFile->run($moduleName, $dirInfo, $repositoryName);

        $this->consoleOutput->success('The '. $moduleName  .' module\'s validation file generation is successful');
    }
}