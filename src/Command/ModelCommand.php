<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Shadon\DevTools\BuildFile\ModelFile;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ModelCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectPath',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'model user --name=info';
        $this->setDescription('模型层文件生成')
            ->setHelp($help)
            ->addArgument('module', InputArgument::REQUIRED, '模块名')
            ->addOption('name', null, InputOption::VALUE_OPTIONAL, '模型文件名');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('module');
        $modelName  = $input->getOption('name') ? [$input->getOption('name')] : [];
        $modelFile = new ModelFile($this->getDI());
        $projectPath = $this->projectPath;

        $moduleDir = sprintf('%s/src/%s',
                $projectPath,
                ucfirst($moduleName)
            );
        $namespace = sprintf('%s\\Model\Mysql',
            ucfirst($moduleName)
            );
        $dirInfo = [
            'path' => $moduleDir . '/Model/Mysql',
            'namespace' => $namespace,
            'moduleDir' => $moduleDir,
            'logic' => [
                'path' => $moduleDir . '/Logic',
                'namespace' => strstr($namespace, '\\', true) . '\Logic',
            ]
        ];

        $modelFile->setDirInfo($dirInfo);
        $modelFile->run($moduleName, $modelName);

        $this->consoleOutput->success('The '. $moduleName  .' module\'s model file generation is successful');
    }
}