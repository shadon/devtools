<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArrayInput;

class CreateProjectCommand extends BaseCommand
{
    private $gitAddress= [
        'api' => 'https://gitee.com/shadon/api.git',
        'application' => 'https://gitee.com/shadon/application.git',
    ];

    private $path = '';

    private $type = '';

    private $defaultType = 'api';

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'create-project projectName --type=api';
        $this->setDescription('项目构建')
            ->setHelp($help)
            ->addArgument('name', InputArgument::REQUIRED, '项目名称')
            ->addOption('type', null, InputOption::VALUE_OPTIONAL, '项目类型', $this->defaultType);
    }

    protected function commandInitialize(InputInterface $input, OutputInterface $output)
    {
        $this->shadonEnv->hasEnv('PROJECT_NAMESPACE') && $this->shadonEnv->delEnv('PROJECT_NAMESPACE');
        $this->shadonEnv->hasEnv('PROJECT_SDK_NAMESPACE') && $this->shadonEnv->delEnv('PROJECT_SDK_NAMESPACE');
        $this->shadonEnv->hasEnv('PROJECT_SDK_PATH') && $this->shadonEnv->delEnv('PROJECT_SDK_PATH');
        if ('application' === $input->getOption('type')){
            $this->registerCheckMethods[] = 'checkProjectNamespace';
        }
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $projectName = $input->getArgument('name');
        $this->type = $input->getOption('type') ?: $this->defaultType;
        $process = new Process($this->getCommandLine($projectName));
        $process->run();
        if(!$process->isSuccessful()){
            throw new ProcessFailedException($process);
        }

        $this->setProjectSetting();
        $io = new SymfonyStyle($input, $output);
        $outputMessage = sprintf('Project %s Creating a successful,The project path is %s' . PHP_EOL,
                $projectName,
                $this->path
            );
        'api' === $this->type && $this->initSdk();
        $io->success($outputMessage);
    }

    private function getCommandLine(string $projectName): string
    {
        $this->path = getcwd() . '/../' . $projectName;
        $gitAddress = $this->gitAddress[$this->type] ?? '';
        throwIf(empty($gitAddress), RuntimeException::class, '非法的项目类型');

        $commandLine = sprintf('git clone --depth=1 %s %s',
                $gitAddress,
                $this->path
            );

        return $commandLine;
    }

    private function setProjectSetting(): void
    {
        $this->path = realpath($this->path);
        $gitPath = $this->path . '/.git';
        file_exists($gitPath) && unlinkDir($gitPath);
        $this->shadonEnv->setEnv('PROJECT_PATH', $this->path);
        $this->shadonEnv->setEnv('PROJECT_TYPE', $this->type);
    }

    private function initSdk()
    {
        $command = $this->getApplication()->find('sdk');
        $arguments = [
            'command' => 'sdk',
            '--type' => 'init',
        ];
        $input = new ArrayInput($arguments);
        $command->run($input, $this->consoleOutput);
    }
}