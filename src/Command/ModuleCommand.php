<?php

declare(strict_types=1);

namespace Shadon\DevTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Shadon\DevTools\BuildFile\ModuleFile;

class ModuleCommand extends BaseCommand
{
    protected $canRunProjectType = [
        'api',
    ];

    protected $registerCheckMethods = [
        'checkProjectType',
        'checkProjectPath',
    ];

    protected $enableEnv = true;

    /**
     * 配置命令
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $help = 'module --name=user';
        $this->setDescription('模块构建')
            ->setHelp($help)
            ->addOption('name', null, InputOption::VALUE_OPTIONAL, 'Module name');
    }

    /**
     * 命令执行的逻辑
     *
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getOption('name');
        $buildModules = isset($moduleName) ? [$moduleName] : $this->getDI()->getConfig()->module->buildModules;
        $originalTime = $startTime = microtime(true);
        $startDate = date('Y/m/d H:i:s');
        foreach ($buildModules as $module){
            $dirInfo[$module] = (new ModuleFile($this->getDI()))->setBaseDir($this->projectPath . '/src/')
                ->run($module);
            $endTime = microtime(true);
            $expendTime = sprintf('%0.3f', $endTime - $startTime);
            $output->writeln(sprintf('Module %s Build Success ===> Time:(%.3fs)',
                    $module,
                    $expendTime
                ));
            $startTime = $endTime;
        }
        $output->writeln(PHP_EOL . sprintf('Start Time:(%s)' . PHP_EOL . 'Total Time:(%.3fs)',
                $startDate,
                $endTime - $originalTime
            ));
    }
}