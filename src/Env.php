<?php

declare(strict_types = 1);

namespace Shadon\DevTools;

use Dotenv\Dotenv;
use Symfony\Component\Console\Exception\RuntimeException;

class Env
{
    protected $dotenv = null;

    protected static $path;

    protected static $file;

    protected static $filename;

    protected $envValue;

    private function __construct()
    {
        $this->dotenv = new Dotenv(self::$path, self::$file);
        $this->dotenv->load();
    }

    public static function getInstance(string $path = '', string $file = ''): self
    {
        static $instance;

        $instanceKey = md5($path . $file);
        if (! $instance[$instanceKey] instanceof self) {
            self::$path = $path ?: dirname(__DIR__);
            self::$file = $file ?: '.env';
            self::$filename = rtrim(self::$path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . self::$file;
            !file_exists(self::$filename) && file_put_contents(self::$filename, '');
            $instance[$instanceKey] = new self();
        }

        return $instance[$instanceKey];
    }

    public function hasEnv(string $varName): bool
    {
        $this->envValue = getenv($varName);

        return $this->envValue ? true : false;
    }

    public function getEnv(string $varName): string
    {
        return $this->hasEnv($varName) ? $this->envValue : '';
    }

    public function setEnv(string $varName, $value): bool
    {
        $envContents = file_get_contents(self::$filename);
        if (! $this->hasEnv($varName)) {
            $setContents = sprintf(($envContents ? PHP_EOL : '') . '%s=%s',
                    $varName,
                    $value
                );
            $envContents = ($envContents ? rtrim($envContents, PHP_EOL) : '') . $setContents;
        } else {
            $envContents = preg_replace("/({$varName}=).+(\s*)/", '${1}' . $value . '${2}', $envContents);
        }
        $result = file_put_contents(self::$filename, $envContents);
        $this->dotenv->overload();

        return (bool)$result;
    }

    public function delEnv(string $varName): bool
    {
        throwIf(!$this->hasEnv($varName), RuntimeException::class, 'Environment variables ' . $varName . ' do not exist');

        $envContents = file_get_contents(self::$filename);
        $envContents = preg_replace("/({$varName}=).+(\s*)/", '', $envContents);

        $result = file_put_contents(self::$filename, $envContents);
        $this->dotenv->overload();
        putenv($varName);

        return (bool)$result;
    }
}

