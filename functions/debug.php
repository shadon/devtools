<?php

declare(strict_types=1);


if(!function_exists('dd')){
    function dd()
    {
        array_map(function($args){
            dump($args);
        }, func_get_args());
        exit();
    }
}