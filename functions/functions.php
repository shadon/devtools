<?php

declare(strict_types=1);

require __DIR__ . '/debug.php';

if (!function_exists('throwIf')) {
    /**
     * Throw the given exception if the given boolean is true.
     *
     * @param bool              $boolean
     * @param \Throwable|string $exception
     * @param array             ...$parameters
     */
    function throwIf($boolean, $exception, ...$parameters): void
    {
        if ($boolean) {
            throw (is_string($exception) ? new $exception(...$parameters) : $exception);
        }
    }
}


if (!function_exists('unlinkDir')){
    /**
     * 递归删除目录文件.
     *
     * @param string $dirPath
     */
    function unlinkDir(string $dirPath): void
    {
        if (empty($dirPath)) {
            return;
        }

        $dirInfo = new \DirectoryIterator($dirPath);
        foreach ($dirInfo as $file) {
            if ($file->isDot()) {
                continue;
            }

            if ($file->isFile()) {
                @unlink($file->getPathname());
            } elseif ($file->isDir()) {
                unlinkDir($file->getPathname());
            }
        }
        rmdir($dirPath);
    }
}

if (!function_exists('askToUnlinkDir')){
    function askToUnlinkDir(string $message, string $dirPath): void
    {
        fwrite(STDOUT, $message ?: 'Delete directory?');
        $input = strtolower(trim(fgets(STDIN)));
        'y' === $input && unlinkDir($dirPath);
        exit();
    }
}