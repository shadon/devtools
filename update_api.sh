#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:    update_api.sh
# Date:        2017/12/18
# Author:      shadonTools
# Email:       localhost.shell@gmail.com
# Description: sdk内interface文件变更监听，触发重新生成API文件
# -------------------------------------------------------------------------------


# interface文件变更时间
time=60
# sdk路径
sdkPath='/data/web/api.eelly.dev.git/vendor/eelly/eelly-sdk-php/src/SDK/'
# 开发助手路径
devToolsPath='/data/web/devtools.shadon.dev/devTools.php'
# 变更文件数
changeNum=0

getDir(){
    dirList=$( ls -l $1 | awk '/^d/ {print $NF}' )
}

generationApi(){
	$devToolsPath api $1
}

checkFile(){
	fileTime=$(stat $1 | grep Modify | awk -F '.' '{print $1}' | awk -F ' ' '{print $2" "$3}')
    fileTimestamp=$(date -d "$fileTime" +%s)
    nowTimestamp=$(date +%s)
    timeInterval=$[${nowTimestamp} - ${fileTimestamp}]
    if [ $timeInterval -le $time ]
    then
	    echo $module'下'$1'文件发生变动了,API更新中'
	    apiPath=$sdkPath$module'/Api'
	    if [ -d $apiPath ]
	    then
		    rm -rf $apiPath 
	    fi
	    generationApi $module
		((changeNum++))
    fi
}


getDir $sdkPath

for module in $dirList
do
    getDir $sdkPath$module
    for dir in $dirList
    do
        if [ $dir == 'Service' ]
        then
            servicePath=$sdkPath$module'/'$dir
            cd $servicePath
            for file in $(ls -l)
            do
                if [ -f $file ]
                then
					checkFile $file
                fi
            done
        fi
    done
done

if [ $changeNum == 0 ]
then
	echo '没有文件发生变动'
fi

