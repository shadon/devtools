### 安装
```
git clone https://gitee.com/shadon/devtools
composer install
```

### 使用 

#### 注意事项
1. 不同项目类型可以使用的命令不同
2. 初次构建项目、模块模型层时，需在config/sql下配置模块的表结构sql文件（会根据sql文件在mysql内创建表）
3. 表结构sql文件名格式为:数据库前缀+模块名+.sql
4. sql文件内切换不要出现drop database/table语句， **这个行为很疯狂很危险** 
5. sql文件内的创建表语句格式为CREATE TABLE IF NOT EXISTS table_name
6. 出现因配置信息错误等原因出现的提示是否清除文件的操作时，请慎重选择操作(会删文件)
7. 出现文件构建不了等原因，可以先检查下项目的目录权限
8. 命令参数中涉及到类名、方法名的建议严格区分大小写进行输入 例: --interface=UserInterface
9. 已有项目初始使用本助手时，先进行初始化配置 例: ./devTools.php init

如有其它问题和建议可以直接联系QQ:2070143107，添加时备注shadonDevTools

#### 初始化

配置操作的项目地址、类型、命名空间(使用create-project构建项目时不需要使用初始化init)

./devTools.php init

![输入图片说明](https://gitee.com/uploads/images/2017/1123/142515_ecdd582a_1519102.png "屏幕截图.png")

---
#### 环境变量切换

- 切换操作的项目地址

./devTools.php set-env PROJECT_PATH /data/web/xxx

---
#### 项目构建(构建完整的一套应用文件)

- 构建默认类型项目(默认为api)

./devTools.php create-project projectName

- 构建指定类型项目

./devTools.php create-project projectName --type=application

---

#### 模块构建
- 构建config/module.php配置的模块

./devTools.php module

- 构建指定名称的模块

./devTools.php module --name=user

---

#### 逻辑层文件生成

- 生成指定模块下的所有logic文件

./devTools.php logic user

- 生成指定模块下的单logic文件

./devTools.php logic user --name=info

---
#### 控制器层文件生成

- 生成controller文件

./devTools.php controllerName

---
#### 模型层文件生成

- 生成指定模块下的所有model文件

./devTools.php model user

- 生成指定模块下的单model文件

./devTools.php model user --name=info

---
#### Repository层文件生成

- 生成指定模块下的所有repository文件

./devTools.php repository user

- 生成指定模块下的单repository文件

./devTools.php repository user --name=info

---
#### EventListener层文件生成

- 生成指定模块下的单eventListener文件

./devTools.php event-listener user beforeAdd

---
#### Validation层文件生成

- 生成指定模块下的所有validation文件

./devTools.php validation user

- 生成指定模块下的单validation文件

./devTools.php validation user --name=info

---
#### 单元测试文件生成
- 生成config/module.php配置的模块单元测试文件

./devTools.php create-phpunit

- 生成指定模块下的单元测试文件

./devTools.php create-phpunit user

---
#### SDK构建

- 项目SDK初始化

./devTools.php sdk --type=init

- 生成指定模块的SDK文件

./devTools.php sdk --module=user

---
#### API构建

- 生成指定模块的API文件

./devTools.php api user

---
#### 新增/更新接口信息到数据库

- 新增/更新模块下的所有接口信息

./devTools.php api-acl user

- 新增/更新模块下指定接口名的接口信息

./devTools.php api-acl user --interface=info

- 新增/更新模块下指定接口名和方法名的接口信息

./devTools.php api-acl user --interface=info --method=getInfo

注：

接口方法请求参数可选属性的设置：

主参数设置为默认值参数即可
如下图:

![输入图片说明](https://gitee.com/uploads/images/2017/1122/171303_25a0a384_1519102.png "屏幕截图.png")

子参数需在注释的参数类型中添加null描述(不区分大小写)，只有子参数设置才有效
如下图：

![输入图片说明](https://gitee.com/uploads/images/2017/1122/171533_353c9860_1519102.png "屏幕截图.png")

--- 
#### 新增/更新模块的所有acl信息到数据库
- 新增/更新config/module.php配置的模块的acl信息

./devTools.php module-acl

- 新增/更新指定模块的acl信息

./devTools.php module-acl --module=user

---
#### 生成接口请求参数注释

- 根据接口的@requestExample注解生成请求参数注释(需在方法注释内添加@reqArgs注解)

./devTools.php request-args goods goods getStoreAddress

- 示例

添加@reqArgs注解

![输入图片说明](https://gitee.com/uploads/images/2017/1122/170833_186da373_1519102.png "屏幕截图.png")

生成后结果

![输入图片说明](https://gitee.com/uploads/images/2017/1122/170929_e4461d72_1519102.png "屏幕截图.png")

---
#### 生成接口返回信息注释

- 根据接口的@returnExample注解生成返回信息注释(需在方法注释内添加@explain注解)

./devTools.php return-explain goods goods getStoreAddress

- 示例

添加@explain注解

![输入图片说明](https://gitee.com/uploads/images/2017/1122/171050_b8660ab3_1519102.png "屏幕截图.png")

生成后结果

![输入图片说明](https://gitee.com/uploads/images/2017/1122/171127_dcaab4c9_1519102.png "屏幕截图.png")



